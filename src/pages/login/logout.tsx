import { useEffect } from 'react'

import { useAuth } from 'src/hooks/useAuth'

import { useRouter } from 'next/router'

const Logout = () => {
  const router = useRouter()
  const { logout } = useAuth()
  useEffect(() => {
    logout()
    router.replace('/login')
  }, [])

  return (<></>)
}
export default Logout
