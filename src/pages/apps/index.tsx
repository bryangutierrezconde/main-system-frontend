import axios from 'axios'
import React, { useEffect } from 'react'
import GetAllApp from 'src/components/apps/getapp'
import { useAuth } from 'src/hooks/useAuth'

const AppsPage = () => {

  const { logout } = useAuth()
  useEffect(() => {
    const intervalId = setInterval(async () => {
      try {
        const isBlocked = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/${window.localStorage.getItem("id")}`)
        console.log("user: ", isBlocked)
        if (isBlocked.data.isLocked) {
          logout()
        }
        else {
          console.log("aun no esta bloqueado")
        }
      } catch (error: any) {
        console.log(error.response.data.message)
      }
    }, 5000);

    // Devuelve una función de limpieza para detener el intervalo cuando el componente se desmonta
    return () => clearInterval(intervalId);
  }, []);

  return (
    <>
      <GetAllApp />
    </>
  )
}
export default AppsPage
