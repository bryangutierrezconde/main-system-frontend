// ** MUI Imports
import Grid from '@mui/material/Grid'

const ACLPage = () => {
  // ** Hooks
  // const ability = useContext(AbilityContext)

  return (
    <Grid>
      <h1>Este es un ejemplo de la pagina de roles</h1>
    </Grid>
  )
}

ACLPage.acl = {
  action: 'read',
  subject: 'acl-page'
}

export default ACLPage
