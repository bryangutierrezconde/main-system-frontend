import { Box, Tabs, Tab } from '@mui/material';
import axios from 'axios';
import React, { useEffect } from 'react'
import GetAllPermissions from 'src/components/permisos/getPermisos';
import { useAuth } from 'src/hooks/useAuth';

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}


const PermisosPage: React.FC = () => {

  const { logout } = useAuth()
  useEffect(() => {
    const intervalId = setInterval(async () => {
      try {
        const isBlocked = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/${window.localStorage.getItem("id")}`)
        console.log("user: ", isBlocked)
        if (isBlocked.data.isLocked) {
          logout()
        }
        else {
          console.log("aun no esta bloqueado")
        }
      } catch (error: any) {
        console.log(error.response.data.message)
      }
    }, 10 * 60 * 1000);

    // Devuelve una función de limpieza para detener el intervalo cuando el componente se desmonta
    return () => clearInterval(intervalId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: '100%' }}>
      <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
          variant="scrollable"
          scrollButtons="auto">
          <Tab label="Central" {...a11yProps(0)} />
          <Tab label="Personal" {...a11yProps(1)} />
          <Tab label="Activos" {...a11yProps(2)} />
          <Tab label="Biblioteca" {...a11yProps(3)} />
          <Tab label="Gestion Documental" {...a11yProps(4)} />
        </Tabs>
      </Box>
      <GetAllPermissions name={'central'} title={'Central'} index={0} value={value} />
      <GetAllPermissions name={'personal'} title={'Personal'} index={1} value={value} />
      <GetAllPermissions name={'activo'} title={'Activos'} index={2} value={value} />
      <GetAllPermissions name={'biblioteca'} title={'Biblioteca'} index={3} value={value} />
      <GetAllPermissions name={'gestiondocumental'} title={'Gestion Documental'} index={4} value={value} />
    </Box>
  )
}
export default PermisosPage
