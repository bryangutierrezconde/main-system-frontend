import axios from 'axios';
import React, { useEffect } from 'react'
import GetAllRol from 'src/components/roles/getrol'
import { useAuth } from 'src/hooks/useAuth';

const RolPage: React.FC = () => {

  const { logout } = useAuth()

  useEffect(() => {
    const intervalId = setInterval(async () => {
      try {
        const isBlocked = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/${window.localStorage.getItem("id")}`)
        console.log("user: ", isBlocked)
        if (isBlocked.data.isLocked) {
          logout()
        }
        else {
          console.log("aun no esta bloqueado")
        }
      } catch (error: any) {
        console.log(error.response.data.message)
      }
    }, 10 * 60 * 1000);

    // Devuelve una función de limpieza para detener el intervalo cuando el componente se desmonta
    return () => clearInterval(intervalId);
  }, []);

  return <GetAllRol />
}
export default RolPage
