import { Box, Button, Card, Collapse, Grid, Link, MenuItem, Select, SelectChangeEvent, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TextField, Tooltip, Typography, styled } from '@mui/material'
import CircularProgress from '@mui/material/CircularProgress'
import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { UIContext } from 'src/context/ui'
import CustomAvatar from 'src/@core/components/mui/avatar'
import { ThemeColor } from 'src/@core/layouts/types'
import { getInitials } from 'src/@core/utils/get-initials'
import 'react-toastify/dist/ReactToastify.css';
import toast from 'react-hot-toast'
import CustomChip from 'src/@core/components/mui/chip'
import AddPassword from 'src/components/users/addpassword'
import { findPermission } from 'src/components/permisos'

interface user {
  id: string
  name: string
  lastName: string
  ci: string
  email: string
  phone: string
  address: string
  nationality: string
  unity: string
  charge: string
  schedule: string
  file: string
  isActive: boolean
  avatarColor?: ThemeColor
  password: string
}


const StyledLink = styled(Link)(({ theme }) => ({
  fontWeight: 600,
  fontSize: '1rem',
  cursor: 'pointer',
  textDecoration: 'none',
  color: theme.palette.text.secondary,
  '&:hover': {
    color: theme.palette.primary.main
  }
}))

const convertBase64ToImageUrl = (base64String: string) => {
  return `data:image/png;base64,${base64String}`
}

const renderClient = (row: user) => {
  const imageSrc = convertBase64ToImageUrl(row.file)

  if (row.file) {
    // return <img src={imageSrc} style={{ width: '34px', height: '34px' }} />;
    return <CustomAvatar src={imageSrc} sx={{ mr: 3, width: 34, height: 34 }} />
  } else {
    return (
      <CustomAvatar
        skin='light'
        color={row.avatarColor || 'primary'}
        sx={{ mr: 3, width: 34, height: 34, fontSize: '1rem' }}
      >
        {getInitials(row.name ? row.name : '')}
      </CustomAvatar>
    )
  }
}

interface UserStatusType {
  [key: string]: ThemeColor
}
const userStatusObj: UserStatusType = {
  activo: 'success',
  pending: 'warning',
  inactivo: 'error'
}
const SetPasswordUser = () => {
  const { getData, closeData } = useContext(UIContext)
  const [pageSize, setPageSize] = useState<number>(10)
  const [total, setTotal] = useState<number>()
  const [stateMenu, setStateMenu] = useState("1")
  const [isActive, setIsActive] = useState<boolean>(true)
  const [fullName, setFullName] = useState<string>("")
  const [ci, setCi] = useState<string>("")
  const [email, setEmail] = useState<string>("")
  const [phone, setPhone] = useState<string>("")
  const [page, setPage] = useState(0)
  const [user, setUser] = useState<user[]>([])
  const [isLoading, SetIsLoading] = useState<boolean>(true)

  const [checked, setChecked] = React.useState(false);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };

  useEffect(() => {
    get();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageSize, page, isActive]);

  useEffect(() => {
    if (getData) {
      get();
      closeData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getData, closeData]);

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    console.log("handleChangePage ", newPage)
    setPage(newPage)
  }
  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    console.log("handleChangeRowsPerPage ", event.target.value)
    setPageSize(parseInt(event.target.value));
    setPage(0);
  };
  const HandleIsActive = (event: SelectChangeEvent) => {
    if (event.target.value == "1") {
      setIsActive(true)
    }
    if (event.target.value == "2") {
      setIsActive(false)
    }
    setStateMenu(event.target.value)
  };
  const get = async () => {
    try {
      console.log("get")
      SetIsLoading(true)
      const response = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}user?page=${page + 1}&limit=${pageSize}&isActive=${isActive}&fullName=${fullName}&ci=${ci}&email=${email}&phone=${phone}`)
      setUser(response.data.data)
      setTotal(response.data.total)
      console.log(response.data)
      SetIsLoading(false)
    } catch (error: any) {
      toast.error(error.response.data.message)
      console.log(error)
    }
  }
  const reset = () => {
    setIsActive(true)
    setFullName("")
    setEmail("")
    setPhone("")
    setCi("")
    get()
  }



  return (
    <>
      {findPermission('CENTRAL_ESTABLECER_APP_USUARIO') ? (
        <Card >
          <Grid container spacing={1}>
            <Grid item xs={12}><Button
              onClick={handleChange}
              sx={{ mb: 2.5 }}
              variant='contained'
              style={{ float: 'left', marginTop: 30, marginLeft: 20 }}>
              Mostrar Filtros
            </Button>
            </Grid>
            <Grid item
              xs={12}
              style={{ padding: 15 }}>
              <Collapse in={checked}>
                <Grid container spacing={2}>
                  <Grid item xs={2}>
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      label="Estado"
                      value={stateMenu}
                      onChange={HandleIsActive}
                    >
                      <MenuItem value={1}>Activo</MenuItem>
                      <MenuItem value={2}>Inactivo</MenuItem>
                    </Select>
                  </Grid>
                  <Grid item xs={3}>
                    <TextField id="outlined-basic" label="Nombre Completo" variant="outlined"
                      value={fullName}
                      onChange={(e) => {
                        setFullName(e.target.value)
                        console.log("fullName lenght: ", fullName.length)

                        // if (fullName.length > 2) {
                        //   setPage(0)
                        //   get()
                        // }
                        // setPage(0)
                        // get()

                      }} />
                  </Grid>
                  <Grid item xs={3}>
                    <TextField id="outlined-basic" label="Email" variant="outlined"
                      value={email}
                      onChange={(e) => {
                        setEmail(e.target.value)
                        console.log("email lenght: ", email.length, " ", email)

                        // if (fullName.length > 2) {
                        //   setPage(0)
                        //   get()
                        // }
                        // setPage(0)
                        // get()
                      }} />
                  </Grid>
                  <Grid item xs={2}>
                    <TextField id="outlined-basic" label="Celular" variant="outlined"
                      value={phone}
                      onChange={(e) => {
                        setPhone(e.target.value)
                        console.log("phone lenght: ", phone.length)

                        // if (fullName.length > 2) {
                        //   setPage(0)
                        //   get()
                        // }
                        // setPage(0)
                        // get()

                      }} />
                  </Grid>
                  <Grid item xs={2}>
                    <TextField id="outlined-basic" label="C.I." variant="outlined"
                      value={ci}
                      onChange={(e) => {
                        setCi(e.target.value)
                        console.log("CI lenght: ", ci.length)

                        // if (ci.length > 2) {
                        //   setPage(0)
                        //   get()
                        // }
                        // setPage(0)
                        // get()
                      }} />
                  </Grid>
                  <Grid item xs={12}>
                    <Button
                      sx={{ mb: 2.5 }}
                      variant='contained'
                      style={{
                        marginTop: 10,
                        marginRight: 10
                      }} onClick={get}>
                      Buscar
                    </Button>
                    <Button
                      sx={{ mb: 2.5 }}
                      variant='contained'
                      style={{
                        marginTop: 10,
                        marginRight: 10
                      }} onClick={reset}>
                      Restablecer
                    </Button>
                  </Grid>
                </Grid>
              </Collapse>
            </Grid>
          </Grid>
          <Grid item xs={12}>
            <TableContainer>
              <table style={{ width: '100%', margin: 'auto' }}>
                <TableHead>
                  <TableRow>
                    <TableCell style={{ width: "10%" }}>
                      <Typography variant='subtitle2' textAlign={'center'} >
                        Estado
                      </Typography>
                    </TableCell>
                    <TableCell style={{ width: "20%" }}>
                      <Typography variant='subtitle2' >
                        Nombre Completo
                      </Typography>
                    </TableCell>
                    <TableCell style={{ width: "10%" }}>
                      <Typography variant='subtitle2' >
                        CI
                      </Typography>
                    </TableCell>
                    <TableCell style={{ width: "20%" }}>
                      <Typography variant='subtitle2' >
                        Email
                      </Typography>
                    </TableCell>
                    <TableCell style={{ width: "10%" }}>
                      <Typography variant='subtitle2' >
                        Celular
                      </Typography>
                    </TableCell>
                    <TableCell style={{ width: "10%" }}>
                      <Typography variant='subtitle2' >
                        Nacionalidad
                      </Typography>
                    </TableCell>
                    <TableCell style={{ width: "10%" }}>
                      <Typography variant='subtitle2' >
                        Direccion
                      </Typography>
                    </TableCell>
                    <TableCell style={{ width: "10%" }}>
                      <Typography variant='subtitle2' textAlign={'center'} >
                        Contraseña
                      </Typography>
                    </TableCell>
                  </TableRow>
                </TableHead>
                {isLoading ? (
                  <TableBody>
                    <TableRow>
                      <TableCell colSpan={12} align="center">
                        <CircularProgress color='primary' />
                      </TableCell>
                    </TableRow>
                  </TableBody>) : (
                  <TableBody>
                    {user?.map((data) => (
                      <TableRow key={data.id}
                        hover>
                        <TableCell style={{ width: "10%" }}>
                          <CustomChip
                            skin='light'
                            size='small'
                            label={data.isActive ? 'activo' : 'inactivo'}
                            color={userStatusObj[data.isActive ? 'activo' : 'inactivo']}
                            sx={{ textTransform: 'capitalize', '& .MuiChip-label': { lineHeight: '18px' } }}
                          />
                        </TableCell>
                        <TableCell style={{ width: '20%' }}>
                          <Tooltip title={data.name + ' ' + data.lastName} arrow>
                            <Box sx={{ display: 'flex', alignItems: 'center' }}>
                              {data.file ? (
                                <CustomAvatar src={convertBase64ToImageUrl(data.file)} sx={{ mr: 3, width: 34, height: 34 }} />
                              ) : (
                                <CustomAvatar
                                  skin='light'
                                  color={data.avatarColor || 'primary'}
                                  sx={{ mr: 3, width: 34, height: 34, fontSize: '1rem' }}
                                >
                                  {getInitials(data.name ? data.name : '')}
                                </CustomAvatar>
                              )}

                              <Box sx={{ display: 'flex', alignItems: 'flex-start', flexDirection: 'column' }}>
                                <div
                                  style={{
                                    whiteSpace: 'nowrap',
                                    overflow: 'hidden',
                                    textOverflow: 'ellipsis',
                                    maxWidth: '150px',
                                  }}
                                >
                                  <Typography variant='subtitle2'>{[data.name, ' ', data.lastName]}</Typography>
                                </div>
                              </Box>
                            </Box>
                          </Tooltip>
                        </TableCell>
                        <TableCell style={{ width: '10%' }}>
                          <Tooltip title={data.ci || ''}>
                            <div style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
                              <Typography noWrap variant='body2'>
                                {data.ci}
                              </Typography>
                            </div>
                          </Tooltip>
                        </TableCell>
                        <TableCell style={{ width: '20%' }}>
                          <Tooltip title={data.email || ''}>
                            <div style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
                              <Typography noWrap variant='body2'>
                                {data.email && `${data.email.slice(0, 10)}...`}
                              </Typography>
                            </div>
                          </Tooltip>
                        </TableCell>

                        <TableCell style={{ width: '10%' }}>
                          <Tooltip title={data.phone || ''}>
                            <div style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
                              <Typography noWrap variant='body2'>
                                {data.phone && `${data.phone.slice(0, 8)}...`}
                              </Typography>
                            </div>
                          </Tooltip>
                        </TableCell>
                        <TableCell style={{ width: '10%' }}>
                          <Tooltip title={data.nationality || ''}>
                            <div style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
                              <Typography noWrap variant='body2'>
                                {data.nationality}
                              </Typography>
                            </div>
                          </Tooltip>
                        </TableCell>
                        <TableCell style={{ width: '10%' }}>
                          <Tooltip title={data.address || ''}>
                            <div style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
                              <Typography noWrap variant='body2'>
                                {data.address && `${data.address.slice(0, 10)}...`}
                              </Typography>
                            </div>
                          </Tooltip>
                        </TableCell>

                        <TableCell style={{ width: '10%', textAlign: 'center' }}>
                          {
                            data.password != null ? (

                              // <Typography textAlign={'center'} style={{ backgroundColor: '#77dd77', padding: 5, borderRadius: 5 }}>Asignada</Typography>
                              <AddPassword id={data.id} email={data.email} isAssigned={true} />
                            ) : (
                              <AddPassword id={data.id} email={data.email} isAssigned={false} />
                            )
                          }
                        </TableCell >

                      </TableRow>
                    ))}
                  </TableBody>
                )}
              </table>
            </TableContainer>
            <Grid item xs={12}>
              <TablePagination
                component="div"
                count={total !== undefined ? total : 100}
                page={page}
                onPageChange={handleChangePage}
                rowsPerPage={pageSize}
                onRowsPerPageChange={handleChangeRowsPerPage}
              />
            </Grid>
          </Grid>
        </Card >

      ) : (
        <>
          <Typography variant='h1'>NO TIENES ACCESSO PARA VER LOS USUARIOS</Typography >
        </>
      )
      }
    </>
  )
}
export default SetPasswordUser


