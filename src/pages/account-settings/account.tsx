// ** React Imports
import { useState, ElementType, ChangeEvent, useEffect } from 'react'

// ** MUI Imports
import Box from '@mui/material/Box'
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import Select from '@mui/material/Select'
import Dialog from '@mui/material/Dialog'
import Divider from '@mui/material/Divider'
import { styled } from '@mui/material/styles'
import Checkbox from '@mui/material/Checkbox'
import MenuItem from '@mui/material/MenuItem'
import TextField from '@mui/material/TextField'
import Typography from '@mui/material/Typography'
import InputLabel from '@mui/material/InputLabel'
import CardHeader from '@mui/material/CardHeader'
import FormControl from '@mui/material/FormControl'
import CardContent from '@mui/material/CardContent'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import FormHelperText from '@mui/material/FormHelperText'
import InputAdornment from '@mui/material/InputAdornment'
import Button, { ButtonProps } from '@mui/material/Button'
import FormControlLabel from '@mui/material/FormControlLabel'

// ** Third Party Imports
import { useForm, Controller } from 'react-hook-form'

// ** Icon Imports
import Icon from 'src/@core/components/icon'
import axios from 'axios'
import { useAuth } from 'src/hooks/useAuth'

interface user {
  _id: string
  name: string
  lastname: string
  gender: string
  ci: string
  email: string
  phone: string
  address: string
  nationality: string
  unity: string
  charge: string
  schedule: string
  level: number
  isActive: boolean
  file: string
  createdAt: string
  updatedAt: string
}
interface Data {
  email: string
  state: string
  address: string
  country: string
  lastName: string
  currency: string
  language: string
  timezone: string
  firstName: string
  organization: string
  number: number | string
  zipCode: number | string
}

const initialData: Data = {
  state: '',
  number: '',
  address: '',
  zipCode: '',
  lastName: 'Doe',
  currency: 'usd',
  firstName: 'John',
  language: 'arabic',
  timezone: 'gmt-12',
  country: 'australia',
  email: 'john.doe@example.com',
  organization: 'ThemeSelection'
}

const ImgStyled = styled('img')(({ theme }) => ({
  width: 120,
  height: 120,
  marginRight: theme.spacing(5),
  borderRadius: theme.shape.borderRadius
}))

const ButtonStyled = styled(Button)<ButtonProps & { component?: ElementType; htmlFor?: string }>(({ theme }) => ({
  [theme.breakpoints.down('sm')]: {
    width: '100%',
    textAlign: 'center'
  }
}))

const ResetButtonStyled = styled(Button)<ButtonProps>(({ theme }) => ({
  marginLeft: theme.spacing(4),
  [theme.breakpoints.down('sm')]: {
    width: '100%',
    marginLeft: 0,
    textAlign: 'center',
    marginTop: theme.spacing(4)
  }
}))

const Account = () => {
  // ** State
  const [open, setOpen] = useState<boolean>(false)
  const [inputValue, setInputValue] = useState<string>('')
  const [userInput, setUserInput] = useState<string>('yes')
  const [formData, setFormData] = useState<Data>(initialData)
  const [imgSrc, setImgSrc] = useState<string>('/images/avatars/1.png')
  const [secondDialogOpen, setSecondDialogOpen] = useState<boolean>(false)

  // ** Hooks
  const {
    control,
    handleSubmit,
    formState: { errors }
  } = useForm({ defaultValues: { checkbox: false } })

  const handleClose = () => setOpen(false)

  const handleSecondDialogClose = () => setSecondDialogOpen(false)

  const onSubmit = () => setOpen(true)

  const handleConfirmation = (value: string) => {
    handleClose()
    setUserInput(value)
    setSecondDialogOpen(true)
  }

  const handleInputImageChange = (file: ChangeEvent) => {
    const reader = new FileReader()
    const { files } = file.target as HTMLInputElement
    if (files && files.length !== 0) {
      reader.onload = () => setImgSrc(reader.result as string)
      reader.readAsDataURL(files[0])

      if (reader.result !== null) {
        setInputValue(reader.result as string)
      }
    }
  }
  const handleInputImageReset = () => {
    setInputValue('')
    setImgSrc('/images/avatars/1.png')
  }

  const handleFormChange = (field: keyof Data, value: Data[keyof Data]) => {
    setFormData({ ...formData, [field]: value })
  }

  const id = window.localStorage.getItem('id')
  const [image, setImage] = useState<string>('')
  const [user, setUser] = useState<user>()
  const [isLoading, SetIsLoading] = useState<boolean>(true)

  useEffect(() => {
    const get = async () => {
      try {
        const response = await axios.get<user>(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/${id}`)
        setUser(response.data)
        if (user?.file !== undefined) {
          const aux = 'data:image/png;base64,' + user.file
          setImage(aux)
        }
        SetIsLoading(false)
        console.log(response.data)
      } catch (error) {
        console.log(error)
      }
    }
    get()
  }, [])



  const { logout } = useAuth()
  useEffect(() => {
    const intervalId = setInterval(async () => {
      try {
        const isBlocked = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/${window.localStorage.getItem("id")}`)
        if (isBlocked.data.isLocked) {
          logout()
        }
        else {
          console.log("aun no esta bloqueado")
        }
      } catch (error: any) {
        console.log(error.response.data.message)
      }
    }, 5000);

    // Devuelve una función de limpieza para detener el intervalo cuando el componente se desmonta
    return () => clearInterval(intervalId);
  }, []);

  return (
    <Grid container spacing={6}>
      {/* Account Details Card */}
      <Grid item xs={12}>
        <Card>
          <CardHeader title='Detalles del Usuario' />
          <form>
            <CardContent sx={{ pt: 0 }}>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <ImgStyled src={image} alt='Profile Pic' />
                <div>
                  <ButtonStyled component='label' variant='contained' htmlFor='account-settings-upload-image'>
                    Actualizar foto
                    <input
                      hidden
                      type='file'
                      value={inputValue}
                      accept='image/png, image/jpeg'
                      onChange={handleInputImageChange}
                      id='account-settings-upload-image'
                    />
                  </ButtonStyled>
                  <ResetButtonStyled color='secondary' variant='outlined' onClick={handleInputImageReset}>
                    Reiniciar
                  </ResetButtonStyled>
                </div>
              </Box>
            </CardContent>
            <Divider />
            <CardContent>
              <Grid container spacing={6}>
                <Grid item xs={12} sm={6}>
                  {/* <TextField
                    fullWidth
                    label='First Name'
                    placeholder='John'
                    value={user?.name}
                    onChange={e => handleFormChange('firstName', e.target.value)}
                  /> */}
                  <label style={{ marginLeft: 15 }}>Nombres</label>
                  <Typography variant='h6' style={{ margin: 10, padding: 10, borderRadius: 10, borderStyle: 'solid', borderColor: '#f2e8e1' }}>
                    {user?.name}
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* <TextField
                    fullWidth
                    label='Last Name'
                    placeholder='Doe'
                    value={user?.lastname}
                    onChange={e => handleFormChange('lastName', e.target.value)}
                  /> */}
                  <label style={{ marginLeft: 15 }}>Apellidos</label>
                  <Typography variant='h6' style={{ margin: 10, padding: 10, borderRadius: 10, borderStyle: 'solid', borderColor: '#f2e8e1' }}>
                    {user?.lastname}
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* <TextField
                    fullWidth
                    type='email'
                    label='Email'
                    value={formData.email}
                    placeholder='john.doe@example.com'
                    onChange={e => handleFormChange('email', e.target.value)}
                  /> */}
                  <label style={{ marginLeft: 15 }}>Correo Electronico</label>
                  <Typography variant='h6' style={{ margin: 10, padding: 10, borderRadius: 10, borderStyle: 'solid', borderColor: '#f2e8e1' }}>
                    {user?.email}
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <label style={{ marginLeft: 15 }}>Genero</label>
                  <Typography variant='h6' style={{ margin: 10, padding: 10, borderRadius: 10, borderStyle: 'solid', borderColor: '#f2e8e1' }}>
                    {user?.gender}
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <label style={{ marginLeft: 15 }}>C.I.</label>
                  <Typography variant='h6' style={{ margin: 10, padding: 10, borderRadius: 10, borderStyle: 'solid', borderColor: '#f2e8e1' }}>
                    {user?.ci}
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <label style={{ marginLeft: 15 }}>Direccion</label>
                  <Typography variant='h6' style={{ margin: 10, padding: 10, borderRadius: 10, borderStyle: 'solid', borderColor: '#f2e8e1' }}>
                    {user?.address}
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <label style={{ marginLeft: 15 }}>Cargo</label>
                  <Typography variant='h6' style={{ margin: 10, padding: 10, borderRadius: 10, borderStyle: 'solid', borderColor: '#f2e8e1' }}>
                    {user?.charge}
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <label style={{ marginLeft: 15 }}>Unidad</label>
                  <Typography variant='h6' style={{ margin: 10, padding: 10, borderRadius: 10, borderStyle: 'solid', borderColor: '#f2e8e1' }}>
                    {user?.unity}
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <label style={{ marginLeft: 15 }}>Nacionalidad</label>
                  <Typography variant='h6' style={{ margin: 10, padding: 10, borderRadius: 10, borderStyle: 'solid', borderColor: '#f2e8e1' }}>
                    {user?.nationality}
                  </Typography>
                </Grid>
                <Grid item xs={12} sm={6}>
                  <label style={{ marginLeft: 15 }}>Nivel</label>
                  <Typography variant='h6' style={{ margin: 10, padding: 10, borderRadius: 10, borderStyle: 'solid', borderColor: '#f2e8e1' }}>
                    {user?.level}
                  </Typography>
                </Grid>

                {/* <Grid item xs={12}>
                  <Button variant='contained' sx={{ mr: 3 }}>
                    Save Changes
                  </Button>
                  <Button type='reset' variant='outlined' color='secondary' onClick={() => setFormData(initialData)}>
                    Reset
                  </Button>
                </Grid> */}
              </Grid>
            </CardContent>
          </form>
        </Card>
      </Grid>
    </Grid>
  )
}

export default Account
