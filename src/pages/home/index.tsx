import { Grid } from '@mui/material'
import axios from 'axios'

import { useEffect } from 'react'
import GetAssets from 'src/components/dashboard/getAssets'

// import GetUserAttendance from 'src/components/dashboard/getAttendance'
import GetDocumentUser from 'src/components/dashboard/getDocuments'
import GetUserSchedule from 'src/components/dashboard/getSchedule'
import GetUserScheduleAll from 'src/components/dashboard/getScheduleAll'
import { findSystem } from 'src/components/sistemas'
import { useAuth } from 'src/hooks/useAuth'
import toast from 'react-hot-toast'
import 'react-toastify/dist/ReactToastify.css';

const Home = () => {

  const { logout } = useAuth()

  useEffect(() => {
    const intervalId = setInterval(async () => {
      try {
        const isBlocked = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/${window.localStorage.getItem("id")}`)
        console.log("user: ", isBlocked)
        if (isBlocked.data.isLocked) {
          logout()
        }
        else {
          console.log("aun no esta bloqueado")
        }
      } catch (error: any) {
        toast.error(error.response.data.message)
      }
    }, 10 * 60 * 1000);

    // Devuelve una función de limpieza para detener el intervalo cuando el componente se desmonta
    return () => clearInterval(intervalId);
  }, []);

  return (
    <>
      {/* <GetUserScheduleAll /> */}
      <Grid container spacing={2} flexWrap={'wrap'}>
        {findSystem('ACTIVO') ? (
          <Grid container xs={12} sm={6} md={3} >
            <GetAssets />
          </Grid>
        ) : (
          <></>
        )}
        {/* {findSystem('CENTRAL') ? (
          <Grid item xs={3}>
            <GetUserSchedule />
          </Grid>
        ) : (
          <></>
        )} */}
        {findSystem('CENTRAL') ? (
          <Grid item xs={12} sm={6} md={3}>
            <GetUserSchedule />
          </Grid>
        ) : (
          <></>
        )}
        {/* {findSystem('personal') ? (
          <Grid item xs={4}>
            <GetUserAttendance />
          </Grid>
        ) : (
          <></>
        )} */}
        {findSystem('GESTION-DOCUMENTAL') ? (
          <Grid item xs={3}>
            <GetDocumentUser />
          </Grid>
        ) : (
          <></>
        )}
      </Grid>
    </>
  )
}

export default Home
