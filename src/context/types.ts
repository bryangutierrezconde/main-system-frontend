//import { type } from 'os'

export type ErrCallbackType = (err: { [key: string]: string }) => void

export type LoginParams = {
  email: string
  password: string

  // rememberMe?: boolean
  app: string
}

export type DecodedToken = {
  id: string
  roles: string

  // Otras propiedades que esperas tener en el token decodificado
}
export type AuthValuesType = {
  loading: boolean
  logout: () => void

  setLoading: (value: boolean) => void
  user: DecodedToken | null
  setUser: (value: DecodedToken | null) => void
  login: (params: LoginParams, errorCallback?: ErrCallbackType) => void
}
