import { createContext } from 'react'

interface ContextProps {
  getData: boolean
  openData: () => void
  closeData: () => void
  getSchedule: boolean
  openSchedule: () => void
  closeSchedule: () => void
  getRol: boolean
  openRol: () => void
  closeRol: () => void
}

export const UIContext = createContext({} as ContextProps)
