import { UIState } from '.'

type UIActionType =
  | { type: 'UI- Open UpdateData' }
  | { type: 'UI- Close UpdateData' }
  | { type: 'UI- Open ScheduleData' }
  | { type: 'UI- Close ScheduleData' }
  | { type: 'UI- Open RolData' }
  | { type: 'UI- Close RolData' }

export const uiReducer = (state: UIState, action: UIActionType): UIState => {
  switch (action.type) {
    case 'UI- Open UpdateData':
      console.log('openData')

      return {
        ...state,
        getData: true
      }
    case 'UI- Close UpdateData':
      console.log('closeData')

      return {
        ...state,
        getData: false
      }
    case 'UI- Open ScheduleData':
      console.log('openSchedule')

      return {
        ...state,
        getSchedule: true
      }
    case 'UI- Close ScheduleData':
      console.log('closeSchedule')

      return {
        ...state,
        getSchedule: false
      }
    case 'UI- Open RolData':
      console.log('openRol')

      return {
        ...state,
        getRol: true
      }
    case 'UI- Close RolData':
      console.log('closeRol')

      return {
        ...state,
        getRol: false
      }
  }

  return state
}
