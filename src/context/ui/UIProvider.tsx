/* eslint-disable @typescript-eslint/no-unused-vars */
import { FC, ReactNode, useReducer } from 'react'
import { UIContext, uiReducer } from './'

export interface UIState {
  getData: boolean
  getSchedule: boolean
  getRol: boolean
}

const UI_INITIAL_STATE: UIState = {
  getData: false,
  getSchedule: false,
  getRol: false
}

export const UIProvider: FC<{ children: ReactNode }> = ({ children }) => {
  const [state, dispatch] = useReducer(uiReducer, UI_INITIAL_STATE)
  const openData = () => {
    dispatch({ type: 'UI- Open UpdateData' })
  }
  const closeData = () => {
    dispatch({ type: 'UI- Close UpdateData' })
  }
  const openSchedule = () => {
    dispatch({ type: 'UI- Open ScheduleData' })
  }
  const closeSchedule = () => {
    dispatch({ type: 'UI- Close ScheduleData' })
  }
  const openRol = () => {
    dispatch({ type: 'UI- Open RolData' })
  }
  const closeRol = () => {
    dispatch({ type: 'UI- Close RolData' })
  }

  return (
    <UIContext.Provider value={{ ...state, openData, closeData, openSchedule, closeSchedule, openRol, closeRol }}>
      {children}
    </UIContext.Provider>
  )
}
