// ** React Imports
import { createContext, useEffect, useState, ReactNode } from 'react'

// ** Next Import
import { useRouter } from 'next/router'

// ** Axios
import axios from 'axios'

// ** Config
import authConfig from 'src/configs/auth'

// ** Types
import { AuthValuesType, LoginParams, ErrCallbackType, DecodedToken } from './types'

// ** Defaults
const defaultProvider: AuthValuesType = {
  user: null,
  loading: true,
  setUser: () => null,
  setLoading: () => Boolean,
  login: () => Promise.resolve(),
  logout: () => Promise.resolve()
}

const AuthContext = createContext(defaultProvider)

type Props = {
  children: ReactNode
}

const AuthProvider = ({ children }: Props) => {
  // ** States
  const [user, setUser] = useState<DecodedToken | null>(defaultProvider.user)
  const [loading, setLoading] = useState<boolean>(defaultProvider.loading)

  // ** Hooks
  const router = useRouter()

  useEffect(() => {
    const initAuth = async (): Promise<void> => {
      const storedToken = window.localStorage.getItem(authConfig.storageTokenKeyName)!
      if (storedToken) {
        setLoading(true)

        await axios
          .get(authConfig.meEndpoint, {
            headers: {
              Authorization: storedToken
            }
          })
          .then(async response => {
            setLoading(false)
            setUser({ ...response.data.userData })
          })
          .catch(() => {
            // localStorage.removeItem('refreshToken')
            localStorage.removeItem(authConfig.storageTokenKeyName)
            window.localStorage.removeItem('id')
            window.localStorage.removeItem('CENTRAL')
            window.localStorage.removeItem('PERSONAL')
            window.localStorage.removeItem('ACTIVO')
            window.localStorage.removeItem('BIBLIOTECA')
            window.localStorage.removeItem('GESTION-DOCUMENTAL')
            window.localStorage.removeItem('token-CENTRAL')
            window.localStorage.removeItem('token-PERSONAL')
            window.localStorage.removeItem('token-ACTIVO')
            window.localStorage.removeItem('token-BIBLIOTECA')
            window.localStorage.removeItem('token-GESTION-DOCUMENTAL')
            window.localStorage.removeItem('systems')
            window.localStorage.removeItem('permisos')
            setUser(null)
            setLoading(false)
            if (authConfig.onTokenExpiration === 'logout' && !router.pathname.includes('login')) {
              router.replace('/login')
            }
          })
      } else {
        setLoading(false)
      }
    }

    initAuth()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const handleLogin = (params: LoginParams, errorCallback?: ErrCallbackType) => {
    axios
      .post(authConfig.loginEndpoint, params)
      .then(async response => {
        const returnUrl = router.query.returnUrl

        setUser({ ...response.data.userData })

        const redirectURL = returnUrl && returnUrl !== '/' ? returnUrl : '/'

        // router.replace(redirectURL as string)
      })

      .catch(err => {
        if (errorCallback) errorCallback(err)
      })
  }

  const handleLogout = () => {
    setUser(null)
    localStorage.removeItem(authConfig.storageTokenKeyName)
    window.localStorage.removeItem('id')
    window.localStorage.removeItem('CENTRAL')
    window.localStorage.removeItem('PERSONAL')
    window.localStorage.removeItem('ACTIVO')
    window.localStorage.removeItem('BIBLIOTECA')
    window.localStorage.removeItem('GESTION-DOCUMENTAL')
    window.localStorage.removeItem('token-CENTRAL')
    window.localStorage.removeItem('token-PERSONAL')
    window.localStorage.removeItem('token-ACTIVO')
    window.localStorage.removeItem('token-BIBLIOTECA')
    window.localStorage.removeItem('token-GESTION-DOCUMENTAL')
    window.localStorage.removeItem('systems')
    window.localStorage.removeItem('permisos')
    router.replace('/login')
  }

  const values = {
    user,
    loading,
    setUser,
    setLoading,
    login: handleLogin,
    logout: handleLogout
  }

  return <AuthContext.Provider value={values}>{children}</AuthContext.Provider>
}

export { AuthContext, AuthProvider }
