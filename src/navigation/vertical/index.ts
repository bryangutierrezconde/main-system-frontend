import { VerticalNavItemsType } from 'src/@core/layouts/types'

const navigation = (): VerticalNavItemsType => {
  const systemsString = localStorage.getItem('systems') ?? ''

  const objects: VerticalNavItemsType = [
    {
      title: 'Inicio',
      path: '/home',
      icon: 'mdi:home-outline'
    },
    {
      title: 'Roles y Permisos',
      icon: 'mdi-account-group',
      children: [
        {
          title: 'Administrar Roles',
          path: '/roles',
          icon: 'mdi-clipboard-account'
        },
        {
          title: 'Administrar Permisos',
          path: '/permisos',
          icon: 'mdi-clipboard-plus'
        }
      ]
    },
    {
      title: 'Aplicaciones',
      path: '/apps',
      icon: 'mdi-note-plus-outline'
    },
    {
      title: 'Usuarios',
      icon: 'mdi-account',
      children: [
        {
          title: 'Ver todos los usuarios',
          path: '/users',
          icon: 'mdi-account-search'
        },
        {
          title: 'Asignar una contraseña al usuario',
          path: '/users/setPaswordUser',
          icon: 'mdi-account-star'
        },
        {
          title: 'Asignar aplicaciones al usuario',
          path: '/users/setApp',
          icon: 'mdi-account-settings'
        },
        {
          title: 'Asignar un rol al usuario',
          path: '/users/setRol',
          icon: 'mdi-account-settings-variant'
        },
        {
          title: 'Bloquear Usuario',
          path: '/users/blockUser',
          icon: 'mdi-account-settings-variant'
        }
      ]
    }
  ]

  if (systemsString !== '') {
    const systems: string[] = JSON.parse(systemsString)
    for (let i = systems.length - 1; i >= 0; i--) {
      const system = systems[i]
      if (system != 'CENTRAL') {
        switch (system) {
          case 'PERSONAL':
            objects.push({
              title: system.toLowerCase(),
              path: window.localStorage.getItem(system) || '',
              icon: 'mdi-account-box',
              openInNewTab: true
            })
            break
          case 'ACTIVO':
            objects.push({
              title: system.toLowerCase(),
              path: window.localStorage.getItem(system) || '',
              icon: 'mdi-animation',
              openInNewTab: true
            })
            break
          case 'BIBLIOTECA':
            objects.push({
              title: system.toLowerCase(),
              path: window.localStorage.getItem(system) || '',
              icon: 'mdi-notebook',
              openInNewTab: true
            })
            break
          case 'GESTION-DOCUMENTAL':
            objects.push({
              title: system.toLowerCase(),
              path: window.localStorage.getItem(system) || '',
              icon: 'mdi-book-multiple-variant',
              openInNewTab: true
            })
            break
        }
      }
    }
  }

  return objects
}

export default navigation
