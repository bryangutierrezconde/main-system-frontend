import { Card, Typography, TableRow, TableCell, Grid } from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'

interface Delivery {
  _idAsset: string
  name: string
  code: string
}

const GetUserSchedule = () => {
  const id = window.localStorage.getItem('id')
  const [delivery, setDelivery] = useState<Delivery[]>([])
  const [isLoading, SetIsLoading] = useState<boolean>(true)
  useEffect(() => {
    async function GetDelivery() {
      try {
        const response = await axios.get<Delivery[]>(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/user-assets/${id}`)
        setDelivery(response.data)
        console.log('response: ', response.data)
        SetIsLoading(false)
      } catch (error) {
        console.log('error', error)
      }
    }
    GetDelivery()
  }, [])

  return (
    <>
      {isLoading ? (
        <></>
      ) : (
        <Card>
          <Grid style={{ padding: 10 }} container spacing={2} xs={12} sm={12} md={12}>
            <Grid
              item
              xs={12}
              sm={12}
              md={12}>
              <Typography variant='h6'>ENTREGAS</Typography>
            </Grid>
            {delivery.map(delivery => (
              <>
                <Grid
                  item
                  xs={6}
                  sm={6}
                  md={6}
                >
                  <Typography textAlign={'center'}>{delivery.name}</Typography>
                </Grid>
                <Grid
                  item
                  xs={6}
                  sm={6}
                  md={6}>
                  <Typography textAlign={'center'} variant='subtitle2' sx={{ color: 'success.main' }}>
                    {delivery.code}
                  </Typography>
                </Grid>
              </>
            ))}
          </Grid>
        </Card>
      )}
    </>
  )
}
export default GetUserSchedule
