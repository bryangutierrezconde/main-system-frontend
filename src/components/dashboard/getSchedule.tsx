import { Box, Button, Card, CardContent, CardHeader, Grid, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { UIContext } from 'src/context/ui'

interface Schedule {
  day: number
  name: string
  into: string
  out: string
  intoTwo: string
  outTwo: string
  toleranceInto: number
  toleranceOut: number
  _id: string
}

interface SpecialSchedule {
  day: number
  name: string
  into: string
  out: string
  intoTwo: string
  outTwo: string
  toleranceInto: number
  toleranceOut: number
  permanente: boolean
  dateRange: any[] // Esto debería ser un tipo específico si es relevante
  usersAssigned: string[]
  isActive: boolean
  _id: string
  createdAt: string // Esto debería ser un tipo de fecha válido si lo prefieres
}

interface UserSchedule {
  _id: string
  name: string
  scheduleNormal: Schedule[]
  scheduleSpecial: SpecialSchedule[]
  isActive: boolean
  createdAt: string // Esto debería ser un tipo de fecha válido si lo prefieres
  __v: number
}

const GetUserSchedule = () => {
  const { openSchedule } = useContext(UIContext)
  const id = window.localStorage.getItem('id')
  const [schedule, setSchedule] = useState<UserSchedule>()
  const [isLoading, SetIsLoading] = useState<boolean>(true)
  const fechaActual = new Date()
  const opcionesDia = { weekday: 'long' } // 'long' devuelve el nombre completo del día

  const diaDeLaSemana = fechaActual.toLocaleDateString('es-ES', opcionesDia)

  useEffect(() => {
    async function GetSchedule() {
      try {
        const response = await axios.get<UserSchedule>(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/user-schedule/${id}`)
        setSchedule(response.data)
        console.log('response: ', response.data)
        SetIsLoading(false)
      } catch (error) {
        console.log('error', error)
      }
    }
    GetSchedule()
  }, [])

  const ViewDay = (day: string) => {
    let dayN = 0
    switch (day) {
      case 'lunes':
        dayN = 1
        break
      case 'martes':
        dayN = 2
        break
      case 'miercoles':
        dayN = 3
        break
      case 'jueves':
        dayN = 4
        break
      case 'viernes':
        dayN = 5
        break
      case 'sabado':
        dayN = 6
        break
      case 'domingo':
        dayN = 7
        break
    }
    if (schedule !== undefined) {
      for (let i = 0; i < schedule?.scheduleNormal.length; i++) {
        if (schedule.scheduleNormal[i].day == dayN) {
          return (

            <Card  >
              <CardHeader
                title={day.toUpperCase()} />
              <CardContent>
                <Box >
                  <Grid style={{ padding: 10 }} container spacing={2} xs={12} sm={12} md={12}>

                    <Grid xs={12} sm={12} md={12}><Typography variant='body2' style={{ fontWeight: 'bold', textAlign: 'center' }}
                      gutterBottom={true}
                    >Tiempo de Tolerancia</Typography></Grid>
                    <Grid xs={12} sm={12} md={12}><Typography variant='body2'>
                      <b>Entrada:</b> {schedule.scheduleNormal[i].toleranceInto}mins
                    </Typography>
                      <Typography variant='body2'>
                        <b>Salida:</b> {schedule.scheduleNormal[i].toleranceOut}mins
                      </Typography>
                    </Grid>
                    <Grid xs={8} sm={6} md={9}>
                      <Typography style={{ textAlign: 'center' }} variant='body2'><b>Mañana</b></Typography>
                      <Typography variant='body2' style={{ textAlign: 'center', padding: 5, borderRadius: 10 }}>
                        <b>{schedule.scheduleNormal[i].into}Am - {schedule.scheduleNormal[i].out}Am</b>
                      </Typography>
                      <Typography style={{ textAlign: 'center' }} variant='body2'><b>Tarde</b></Typography>
                      <Typography variant='body2' style={{ textAlign: 'center', padding: 5, borderRadius: 10 }}>
                        <b>{schedule.scheduleNormal[i].intoTwo}Pm - {schedule.scheduleNormal[i].outTwo}Pm</b>
                      </Typography>
                    </Grid>
                    <Grid item xs={4} sm={12} md={3} sx={{ display: 'flex', alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                      <img
                        src={'/images/card-stats-img-1.png'}
                        alt={'image'}
                        height={100}
                        style={{ maxWidth: '100%', height: 'auto' }}
                      />
                    </Grid>

                  </Grid>
                </Box>
              </CardContent>
              {/* <Button onClick={openSchedule}><Typography><b>Ver Horario</b></Typography></Button> */}
            </Card >



          )
        }
      }
    }
  }

  return (

    // <Card style={{ padding: 10 }} >

    // </Card>
    <>
      {isLoading ? (
        <Typography>Cargando...</Typography>
      ) : (
        <>
          {ViewDay(diaDeLaSemana)}

          {/* <dialog open={open}>
            <Button onClick={HandleClose}>Cerrar</Button>
            <TableRow>
              {schedule?.scheduleNormal.map(schedule => (
                <TableCell key={schedule._id}>{day(schedule.day)}</TableCell>
              ))}
            </TableRow>
            <TableRow>
              {schedule?.scheduleNormal.map(schedule => (
                <TableCell key={schedule._id}>
                  <Typography>Tolerancia Entrada</Typography>
                </TableCell>
              ))}
            </TableRow>
            <TableRow>
              {schedule?.scheduleNormal.map(schedule => (
                <TableCell key={schedule._id}>
                  <Typography>{schedule.toleranceInto}</Typography>
                </TableCell>
              ))}
            </TableRow>
            <TableRow>
              {schedule?.scheduleNormal.map(schedule => (
                <TableCell key={schedule._id}>
                  <Typography>Tolerancia Salida</Typography>
                </TableCell>
              ))}
            </TableRow>
            <TableRow>
              {schedule?.scheduleNormal.map(schedule => (
                <TableCell key={schedule._id}>
                  <Typography>{schedule.toleranceOut}</Typography>
                </TableCell>
              ))}
            </TableRow>
            <TableRow>
              {schedule?.scheduleNormal.map(schedule => (
                <TableCell key={schedule._id}>
                  <Typography>Mañana</Typography>
                </TableCell>
              ))}
            </TableRow>
            <TableRow>
              {schedule?.scheduleNormal.map(schedule => (
                <TableCell key={schedule._id}>
                  <Typography>{schedule.into}</Typography>
                </TableCell>
              ))}
            </TableRow>
            <TableRow>
              {schedule?.scheduleNormal.map(schedule => (
                <TableCell key={schedule._id}>
                  <Typography>{schedule.out}</Typography>
                </TableCell>
              ))}
            </TableRow>
            <TableRow>
              {schedule?.scheduleNormal.map(schedule => (
                <TableCell key={schedule._id}>
                  <Typography>Tarde</Typography>
                </TableCell>
              ))}
            </TableRow>
            <TableRow>
              {schedule?.scheduleNormal.map(schedule => (
                <TableCell key={schedule._id}>
                  <Typography>{schedule.intoTwo}</Typography>
                </TableCell>
              ))}
            </TableRow>
            <TableRow>
              {schedule?.scheduleNormal.map(schedule => (
                <TableCell key={schedule._id}>
                  <Typography>{schedule.outTwo}</Typography>
                </TableCell>
              ))}
            </TableRow>
          </dialog> */}
        </>
      )}
    </>
  )
}
export default GetUserSchedule
