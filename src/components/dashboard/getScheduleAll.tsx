import { Button, Card, Grid, TableCell, TableRow, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { UIContext } from 'src/context/ui'

interface Schedule {
  day: number
  name: string
  into: string
  out: string
  intoTwo: string
  outTwo: string
  toleranceInto: number
  toleranceOut: number
  _id: string
}

interface SpecialSchedule {
  day: number
  name: string
  into: string
  out: string
  intoTwo: string
  outTwo: string
  toleranceInto: number
  toleranceOut: number
  permanente: boolean
  dateRange: any[] // Esto debería ser un tipo específico si es relevante
  usersAssigned: string[]
  isActive: boolean
  _id: string
  createdAt: string // Esto debería ser un tipo de fecha válido si lo prefieres
}

interface UserSchedule {
  _id: string
  name: string
  scheduleNormal: Schedule[]
  scheduleSpecial: SpecialSchedule[]
  isActive: boolean
  createdAt: string // Esto debería ser un tipo de fecha válido si lo prefieres
  __v: number
}

const GetUserScheduleAll = () => {
  const { getSchedule, closeSchedule } = useContext(UIContext)
  const id = window.localStorage.getItem('id')
  const [schedule, setSchedule] = useState<UserSchedule>()
  const [isLoading, SetIsLoading] = useState<boolean>(true)

  // const [open, setOpen] = useState<boolean>(false)
  const fechaActual = new Date()
  const opcionesDia = { weekday: 'long' } // 'long' devuelve el nombre completo del día

  const diaDeLaSemana = fechaActual.toLocaleDateString('es-ES', opcionesDia)

  // const HandleOpen = () => {
  //   setOpen(true)
  // }
  // const HandleClose = () => {
  //   setOpen(false)
  // }
  useEffect(() => {
    async function GetSchedule() {
      try {
        const response = await axios.get<UserSchedule>(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/user-schedule/${id}`)
        setSchedule(response.data)
        console.log('response: ', response.data)
        SetIsLoading(false)
      } catch (error) {
        console.log('error', error)
      }
    }
    GetSchedule()
  }, [])

  const day = (day: number) => {
    if (day === 1) return <Typography>Lunes</Typography>
    if (day === 2) return <Typography>Martes</Typography>
    if (day === 3) return <Typography>Miercoles</Typography>
    if (day === 4) return <Typography>Jueves</Typography>
    if (day === 5) return <Typography>Viernes</Typography>
    if (day === 6) return <Typography>Sabado</Typography>
  }

  return (
    <Card style={{ padding: 10 }}>
      {isLoading ? (
        <Typography>Cargando...</Typography>
      ) : (
        <>
          <dialog open={getSchedule}>
            <Card style={{ backgroundColor: 'tomato' }}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Typography textAlign={'center'} variant='h4' fontFamily={'times-new-roman'}>
                    Horario
                  </Typography>
                </Grid>
                <Grid item xs={12} textAlign={'left'}>
                  <Button style={{ backgroundColor: 'white', padding: 5, margin: 10 }} onClick={closeSchedule}>
                    Cerrar
                  </Button>
                </Grid>
              </Grid>

              <Grid item xs={12}>
                <TableRow>
                  {schedule?.scheduleNormal.map(schedule => (
                    <TableCell key={schedule._id}>
                      <Typography textAlign={'center'} variant='subtitle2' fontFamily={'times-new-roman'}>
                        {day(schedule.day)}
                      </Typography>
                    </TableCell>
                  ))}
                </TableRow>
                <TableRow>
                  {schedule?.scheduleNormal.map(schedule => (
                    <TableCell key={schedule._id}>
                      <Typography textAlign={'center'} variant='subtitle2' fontFamily={'times-new-roman'}>
                        Mañana
                      </Typography>
                    </TableCell>
                  ))}
                </TableRow>
                <TableRow>
                  {schedule?.scheduleNormal.map(schedule => (
                    <TableCell key={schedule._id}>
                      <Typography>{schedule.into}</Typography>
                    </TableCell>
                  ))}
                </TableRow>
                <TableRow>
                  {schedule?.scheduleNormal.map(schedule => (
                    <TableCell key={schedule._id}>
                      <Typography>{schedule.out}</Typography>
                    </TableCell>
                  ))}
                </TableRow>
                <TableRow>
                  {schedule?.scheduleNormal.map(schedule => (
                    <TableCell key={schedule._id}>
                      <Typography>Tarde</Typography>
                    </TableCell>
                  ))}
                </TableRow>
                <TableRow>
                  {schedule?.scheduleNormal.map(schedule => (
                    <TableCell key={schedule._id}>
                      <Typography>{schedule.intoTwo}</Typography>
                    </TableCell>
                  ))}
                </TableRow>
                <TableRow>
                  {schedule?.scheduleNormal.map(schedule => (
                    <TableCell key={schedule._id}>
                      <Typography>{schedule.outTwo}</Typography>
                    </TableCell>
                  ))}
                </TableRow>
              </Grid>
            </Card>
          </dialog>
        </>
      )}
    </Card>
  )
}
export default GetUserScheduleAll
