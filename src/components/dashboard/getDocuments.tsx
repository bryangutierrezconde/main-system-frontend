import { Card, Typography } from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { findSystem } from '../sistemas'

interface DocumentationType {
  typeName: string
  idTemplateDocType: string
  activeDocumentType: boolean
}

interface FileRegister {
  _idFile: string
  filename: string
  size: number
  filePath: string
  status: string
  category: string
  extension: string
}
interface UserInfo {
  name: string
  lastName: string
  ci: string
  email: string
  unity: string
}
interface Document {
  _id: string
  numberDocument: string
  userId: string
  title: string
  documentationType: DocumentationType
  stateDocumentUserSend: string
  stateDocumentWorkflow: string
  workflow: null
  description: string
  fileRegister: FileRegister
  fileBase64: string
  active: boolean
  state: string
  comments: string[]
  milestone: string[]
  bitacoraWorkflow: string[]
  idTemplate: string
  base64Template: string
  userInfo: UserInfo
}
const GetDocumentUser = () => {
  const id = window.localStorage.getItem('id')
  const authToken = window.localStorage.getItem('token-gestion-documental')
  const [document, setDocument] = useState<Document[]>([])
  const [isLoading, SetIsLoading] = useState<boolean>(true)
  useEffect(() => {
    if (findSystem('GESTION-DOCUMENTAL')) {
      async function GetDocument() {
        try {
          try {
            const response = await axios.post(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/user-documents/${id}`, {
              token: authToken
            })
            setDocument(response.data)
            console.log('response: ', response.data)
            SetIsLoading(false)
          } catch (error) {
            console.log('error: ', error)
          }
        } catch (error) {
          console.log('error', error)
        }
      }
      GetDocument()
    }
  }, [])

  return (
    <>
      {/* <Card>
        {isLoading ? (
          <></>
        ) : (
          <>
            {document.map(user => (
              <Card
                key={user._id}
                style={{
                  padding: 10
                }}
              >
                <Typography>{user.numberDocument}</Typography>
                <Typography>{user.title}</Typography>
                <Typography>{user.documentationType.typeName}</Typography>
                <Typography>{user.stateDocumentUserSend}</Typography>
                <Typography>{user.description}</Typography>
              </Card>
            ))}
          </>
        )}
      </Card> */}
    </>
  )
}
export default GetDocumentUser
