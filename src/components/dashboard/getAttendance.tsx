import { Card, Typography } from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'

interface Entrances {
  marketHour: string
  infraccion: string
  type: string
  status: string
  shift: string
  _id: string
}
interface AttendanceDetail {
  date: string
  specialDay: string
  entrances: Entrances[]
  exits: string[]
}
interface Attendance {
  _id: string
  name: string
  lastName: string
  ci: string
  schedule: string
  attendanceDetail: AttendanceDetail[]
}
interface UserSchedule {
  _id: string
  name: string
  isActive: boolean
  createdAt: string // Esto debería ser un tipo de fecha válido si lo prefieres
  __v: number
}

const GetUserAttendance = () => {
  const id = window.localStorage.getItem('id')
  const [attendance, setAttendance] = useState<Attendance>()
  const [isLoading, SetIsLoading] = useState<boolean>(true)

  useEffect(() => {
    async function GetSchedule() {
      try {
        const response = await axios.get<Attendance>(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/user-attendance/${id}`)
        if (response.data != undefined) {
          setAttendance(response.data)
          console.log('attendance: ', attendance)
          SetIsLoading(false)
        }
      } catch (error) {
        console.log('error', error)
      }
    }
    GetSchedule()
  }, [])

  return (
    <>
      <Card>
        {isLoading ? (
          <></>
        ) : (
          <Card
            style={{
              padding: 10
            }}
          >
            <Typography variant='h6'>
              {attendance?.name}
              {attendance?.lastName}
            </Typography>
            <Typography>Fecha {attendance?.attendanceDetail[0]?.date}</Typography>
            <Typography>Entrada</Typography>
            <Typography>Ingreso: {attendance?.attendanceDetail[0].entrances[0].marketHour}</Typography>
            <Typography>Infraccion: {attendance?.attendanceDetail[0].entrances[0].infraccion}</Typography>
            <Typography>Estado: {attendance?.attendanceDetail[0].entrances[0].status}</Typography>
          </Card>
        )}
      </Card>
    </>
  )
}

export default GetUserAttendance
