import { Box, Button, Checkbox, Drawer, Grid, Tab, TableCell, TableRow, Tabs, Typography } from '@mui/material'
import axios from 'axios'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import React, { useEffect, useState } from 'react'
import AddBoxIcon from '@mui/icons-material/AddBox'

import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import AddPermissions from './addPermisionsRolAdd';

interface Permission {
  _id: string
  permissionName: string
  isActive: boolean
}



interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function CustomTabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}


const AsignPermissionToRol: React.FC<{ id: string }> = ({ id }) => {
  const [central, setCentral] = useState<Permission[]>([])
  const [personal, setPersonal] = useState<Permission[]>([])
  const [biblioteca, setBiblioteca] = useState<Permission[]>([])
  const [gestiondocumental, setGestionDocumental] = useState<Permission[]>([])
  const [activo, setActivo] = useState<Permission[]>([])
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }


  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };



  const GetPermiso = async () => {
    try {
      const responseCentral = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}permission?limit=500&offset=1&name=central`)
      setCentral(responseCentral.data.data)
      const responsePersonal = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}permission?limit=500&offset=1&name=personal`)
      setPersonal(responsePersonal.data.data)
      const responseBiblioteca = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}permission?limit=500&offset=1&name=biblioteca`)
      setBiblioteca(responseBiblioteca.data.data)
      const responseActivo = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}permission?limit=500&offset=1&name=activo`)
      setActivo(responseActivo.data.data)
      const responseGestionDocumental = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}permission?limit=500&offset=1&name=gestiondocumental`)
      setGestionDocumental(responseGestionDocumental.data.data)
      handleOpen()
    } catch (error: any) {
      toast.error(error.response.data.message)
    }
  }

  // const CheckState: React.FC<{ data: any, name: string }> = ({ data, name }) => {
  const CheckState: React.FC<{ data: any, name: string, index: number, value: number }> = ({ data, index, value }) => {
    const newData: any = []

    const [selectAll, setSelectAll] = useState(false);
    const [selectedRoles, setSelectedRoles] = useState<string[]>([]);

    useEffect(() => {
      console.log("roles actualizados:", selectedRoles);
    }, [selectedRoles]);

    const handleCheckboxChange = (roleId: string) => {
      const updatedRoles = [...selectedRoles];

      if (updatedRoles.includes(roleId)) {
        // Si el rol ya está seleccionado, quítalo
        updatedRoles.splice(updatedRoles.indexOf(roleId), 1);
      } else {
        // Si el rol no está seleccionado, agrégalo
        updatedRoles.push(roleId);
      }

      // Actualiza el estado con la nueva lista de roles seleccionados
      setSelectedRoles(updatedRoles);
    };
    const handleSelectAllChange = () => {
      // Cambia el estado de selectAll
      setSelectAll(!selectAll);

      // Si selectAll es verdadero, selecciona todos los roles; de lo contrario, vacía la lista de roles seleccionados
      if (!selectAll) {
        const roles = newData.map((data: any) => data._id);
        setSelectedRoles(roles);
        console.log("roles", roles);
      } else {
        setSelectedRoles([]);
        console.log("roles", []);
      }
    };

    data.forEach((permiso: any) => {
      console.log("permisos", permiso.permissionName)
      if (permiso.isActive) {
        newData.push({
          _id: permiso._id,
          permissionName: permiso.permissionName
        })
      }
    });

    const FilterName: React.FC<{ name: string }> = ({ name }) => {
      const permission = name.split('_')
      const newName = permission.slice(1).join('_')

      return (
        <Typography variant='subtitle2'>
          {newName}
        </Typography>
      )
    }

    return (

      // <div style={{ width: '100%' }}>
      //   <Accordion >
      //     <AccordionSummary
      //       expandIcon={<ExpandMoreIcon />}
      //       aria-controls="panel1a-content"
      //       id="panel1a-header"
      //     >
      //       <Typography variant='h6' textAlign={'center'}>{name}</Typography>
      //     </AccordionSummary>
      //     <AccordionDetails style={{ overflow: 'hidden', flexDirection: 'column', alignItems: 'center' }}>
      //       <Button sx={{ mb: 2.5 }} variant='contained' style={{ float: 'left' }}>
      //         Mostrar Filtros
      //       </Button>
      //       <TableRow>
      //         <TableCell
      //           style={{ width: '5%' }}>
      //           <Checkbox
      //             checked={selectAll}
      //             onChange={handleSelectAllChange} />
      //         </TableCell>
      //         <TableCell
      //           style={{ width: '5%' }}>
      //           <Typography variant='body2'>Añadir</Typography>
      //         </TableCell>
      //         <TableCell
      //           style={{ width: '90%' }}>
      //           <Typography variant='body2'>Permisos</Typography>
      //         </TableCell>
      //       </TableRow>
      //       {newData.map((data: any) => (
      //         <TableRow key={data._id}>
      //           <TableCell style={{ width: '5%' }}>
      //             <Checkbox
      //               checked={selectAll || selectedRoles.includes(data._id)}
      //               onChange={() => handleCheckboxChange(data._id)} />
      //           </TableCell>
      //           <TableCell style={{ width: '25%' }}>
      //             <AddPermissions id={data._id} allId={selectedRoles} rolId={id} />
      //           </TableCell>
      //           <TableCell style={{ width: '70%' }}>
      //             <FilterName name={data.permissionName} />
      //           </TableCell>
      //         </TableRow>
      //       ))}
      //     </AccordionDetails>
      //   </Accordion>
      // </div>

      <CustomTabPanel value={value} index={index}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Button
              sx={{ mb: 2.5, marginTop: 5 }}
              variant='contained'
              style={{ float: 'left' }}>
              Mostrar Filtros
            </Button>
            <TableRow>
              <TableCell
                style={{ width: '5%' }}>
                <Checkbox
                  checked={selectAll}
                  onChange={handleSelectAllChange} />
              </TableCell>
              <TableCell
                style={{ width: '5%' }}>
                <Typography variant='body2'>Añadir</Typography>
              </TableCell>
              <TableCell
                style={{ width: '90%' }}>
                <Typography variant='body2'>Permisos</Typography>
              </TableCell>
            </TableRow>
            {newData.map((data: any) => (
              <TableRow key={data._id}>
                <TableCell style={{ width: '5%' }}>
                  <Checkbox
                    checked={selectAll || selectedRoles.includes(data._id)}
                    onChange={() => handleCheckboxChange(data._id)} />
                </TableCell>
                <TableCell style={{ width: '25%' }}>
                  <AddPermissions id={data._id} allId={selectedRoles} rolId={id} />
                </TableCell>
                <TableCell style={{ width: '70%' }}>
                  <FilterName name={data.permissionName} />
                </TableCell>
              </TableRow>
            ))}
          </Grid>
          <Grid item xs={12}>
            <Button
              onClick={handleClose}
              sx={{ mb: 2.5 }}
              variant='contained'
              style={{ marginTop: 15 }}>
              Cerrar
            </Button>
          </Grid>
        </Grid>
      </CustomTabPanel >
    )
  }

  return (
    <>
      {findPermission('CENTRAL_EDITAR_ROL') ? (
        <>
          <Button onClick={GetPermiso}>
            <AddBoxIcon style={{ color: "#77dd77" }} />
          </Button>
          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, md: 600, sm: 600, xl: 1000 } } }}
          >
            <Box sx={{ width: '100%' }}>
              <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <Tabs
                  value={value}
                  onChange={handleChange}
                  aria-label="basic tabs example"
                  variant="scrollable"
                  scrollButtons="auto">
                  <Tab label="Central" {...a11yProps(0)} />
                  <Tab label="Personal" {...a11yProps(1)} />
                  <Tab label="Activos" {...a11yProps(2)} />
                  <Tab label="Biblioteca" {...a11yProps(3)} />
                  <Tab label="Gestion Documental" {...a11yProps(4)} />
                </Tabs>
              </Box>
              <CheckState data={central} name={'Central'} value={value} index={0} />
              <CheckState data={personal} name={'Personal'} value={value} index={1} />
              <CheckState data={activo} name={'Activos'} value={value} index={2} />
              <CheckState data={biblioteca} name={'Biblioteca'} value={value} index={3} />
              <CheckState data={gestiondocumental} name={'Gestion Documental'} value={value} index={4} />
            </Box>
          </Drawer>
        </>
      ) : (
        <></>
      )}
    </>
  )
}
export default AsignPermissionToRol
