import { Button, Card, Checkbox, CircularProgress, Collapse, Grid, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TextField, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import EditRol from './editrol'
import { UIContext } from 'src/context/ui'
import DeleteRol from './deleteRol'
import PermisoId from './permisosId'
import NewRol from './newRol'
import AsignPermissionToRol from './addPermissionRol'

import 'react-toastify/dist/ReactToastify.css';
import toast from 'react-hot-toast'
import { ThemeColor } from 'src/@core/layouts/types'
import SearchIcon from '@mui/icons-material/Search';
import CustomChip from 'src/@core/components/mui/chip'

interface PermisionName {
  _id: string
  permissionName: string
}
interface rol {
  _id: string
  rolName: string
  aplicattion: string
  isActive: boolean
  permissionName: PermisionName[]
}

interface UserStatusType {
  [key: string]: ThemeColor
}
const userStatusObj: UserStatusType = {
  activo: 'success',
  pending: 'warning',
  inactivo: 'error'
}

const GetAllRol = () => {
  const { getData, closeData } = useContext(UIContext)
  const [pageSize, setPageSize] = useState<number>(10)
  const [total, setTotal] = useState<number>()
  const [search, setSearch] = useState<string>('')
  const [page, setPage] = useState(0)
  const [user, setUser] = useState<rol[]>([])
  const [isLoading, SetIsLoading] = useState<boolean>(true)

  const [selectAll, setSelectAll] = useState(false);
  const [selectedRoles, setSelectedRoles] = useState<string[]>([]);



  const [checked, setChecked] = React.useState(false);

  const handleChange = () => {
    setChecked((prev) => !prev);
  };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars

  useEffect(() => {
    get();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageSize, page, search]);

  useEffect(() => {
    if (getData) {
      get();
      closeData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getData, closeData]);

  useEffect(() => {
    console.log("roles actualizados:", selectedRoles);
  }, [selectedRoles]);

  const handleCheckboxChange = (roleId: string) => {
    const updatedRoles = [...selectedRoles];

    if (updatedRoles.includes(roleId)) {
      // Si el rol ya está seleccionado, quítalo
      updatedRoles.splice(updatedRoles.indexOf(roleId), 1);
    } else {
      // Si el rol no está seleccionado, agrégalo
      updatedRoles.push(roleId);
    }

    // Actualiza el estado con la nueva lista de roles seleccionados
    setSelectedRoles(updatedRoles);
  };
  const handleSelectAllChange = () => {
    // Cambia el estado de selectAll
    setSelectAll(!selectAll);

    // Si selectAll es verdadero, selecciona todos los roles; de lo contrario, vacía la lista de roles seleccionados
    if (!selectAll) {
      const roles = user.map((rol) => rol._id);
      setSelectedRoles(roles);
      console.log("roles", roles);
    } else {
      setSelectedRoles([]);
      console.log("roles", []);
    }
  };

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    console.log("handleChangePage ", newPage)
    setPage(newPage)
  }
  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    console.log("handleChangeRowsPerPage ", event.target.value)
    setPageSize(parseInt(event.target.value));
    setPage(0);
  };
  const get = async () => {
    try {
      // setSelectedRoles([])
      SetIsLoading(true)
      console.log('limit', pageSize)
      console.log('offset', page + 1)
      console.log('search', search)
      const response = await axios.get(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}rol?limit=${pageSize}&offset=${page + 1}&name=${search}`
      )
      console.log("url: ", process.env.NEXT_PUBLIC_API_CENTRAL, "rol ? limit = ", pageSize, " & offset=", page + 1, " & search=", search)
      setUser(response.data.data)
      setTotal(response.data.total)
      console.log("data", response.data.data)
      console.log('total', response.data.total)
      SetIsLoading(false);
    } catch (error: any) {
      toast.error(error.response.data.message)
      console.log(error)
    }
  }

  return (
    <Card>
      <Grid container spacing={1}>
        {/* <Grid item xs={6}>
          <Typography variant='h4' style={{ margin: 20 }}>Roles</Typography>
        </Grid> */}
        {/* <Grid item xs={6}>
          <Grid container spacing={2}>
            <Grid item xs={2} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
              <SearchIcon />
            </Grid>
            <Grid item xs={10}>
              <TextField
                value={search}
                onChange={(e) => {
                  setSearch(e.target.value)
                  setPage(0)
                }}
                style={{ margin: 20 }}
              ></TextField>
            </Grid>
          </Grid>

        </Grid> */}

        <Grid item xs={12} md={6} sm={6} lg={6}>
          <Button
            onClick={handleChange}
            sx={{ mb: 2.5 }}
            variant='contained'
            style={{ float: 'left', marginTop: 30, marginLeft: 20 }}>
            Mostrar Filtros
          </Button>
        </Grid>

        <Grid item xs={12} md={6} sm={6} lg={6}>
          <NewRol />
        </Grid>
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <Collapse in={checked}>
            <Grid container spacing={2}>
              <Grid item xs={2} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <SearchIcon />
              </Grid>
              <Grid item xs={10}>
                <TextField
                  value={search}
                  onChange={(e) => {
                    setSearch(e.target.value)
                    setPage(0)
                  }}
                  style={{ margin: 20 }}
                ></TextField>
              </Grid>
            </Grid>
          </Collapse>
        </Grid>
        <Grid item xs={12}>
          <TableContainer>
            <table style={{ width: '100%', margin: 'auto' }}>
              <TableHead>
                <TableRow>
                  <TableCell style={{ width: '5%' }}>
                    <Checkbox
                      checked={selectAll}
                      onChange={handleSelectAllChange} />
                  </TableCell>
                  <TableCell style={{ width: "20%" }}>
                    <Typography variant='body2' style={{ textAlign: 'center' }}>
                      Opciones
                    </Typography>
                  </TableCell>
                  <TableCell style={{ width: "15%" }}>
                    <Typography variant='body2' style={{ textAlign: 'center' }}>
                      Estado
                    </Typography>
                  </TableCell>
                  <TableCell style={{ width: "20%" }}>
                    <Typography variant='body2'  >
                      Nombre
                    </Typography>
                  </TableCell>
                  <TableCell style={{ width: "20%" }}>
                    <Typography variant='body2' >
                      Aplicacion
                    </Typography>
                  </TableCell>
                  <TableCell style={{ width: "20%" }}>
                    <Typography variant='body2' style={{ textAlign: 'center' }}>
                      Permisos
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableHead>
              {isLoading ? (
                <TableBody>
                  <TableRow>
                    <TableCell colSpan={12} align="center">
                      <CircularProgress color='primary' />
                    </TableCell>
                  </TableRow>
                </TableBody>
              ) : (<TableBody>
                {user.map((rol) => (
                  <TableRow key={rol._id}
                    hover>
                    <TableCell style={{ width: '5%' }}>
                      <Checkbox
                        checked={selectAll || selectedRoles.includes(rol._id)}
                        onChange={() => handleCheckboxChange(rol._id)} />
                    </TableCell>
                    <TableCell style={{ width: "20%", textAlign: 'center' }}>
                      <EditRol id={rol._id} />
                      <DeleteRol id={rol._id} allId={selectedRoles} />
                    </TableCell>
                    <TableCell style={{ width: "15%", textAlign: 'center' }}>
                      <CustomChip
                        skin='light'
                        size='small'
                        label={rol.isActive ? 'activo' : 'inactivo'}
                        color={userStatusObj[rol.isActive ? 'activo' : 'inactivo']}
                        sx={{ textTransform: 'capitalize', '& .MuiChip-label': { lineHeight: '18px' } }}
                      />
                    </TableCell>
                    <TableCell style={{ width: "20%" }}>
                      <Typography variant='subtitle2' >
                        {rol.rolName}
                      </Typography>
                    </TableCell>
                    <TableCell style={{ width: "20%" }}>
                      <Typography variant='subtitle2' >
                        {rol.aplicattion.toUpperCase()}
                      </Typography>
                    </TableCell>
                    <TableCell style={{ width: "20%", textAlign: 'center' }}>
                      <PermisoId id={rol._id} />
                      <AsignPermissionToRol id={rol._id} />
                    </TableCell>

                  </TableRow>
                ))}
              </TableBody>)}
            </table>
          </TableContainer>
          <Grid item xs={12}>
            <TablePagination
              component="div"
              count={total !== undefined ? total : 100}
              page={page}
              onPageChange={handleChangePage}
              rowsPerPage={pageSize}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Grid>
        </Grid>
      </Grid >

    </Card >
  )
}
export default GetAllRol
