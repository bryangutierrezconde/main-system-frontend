import { Accordion, AccordionDetails, AccordionSummary, Card, Checkbox, CircularProgress, Grid, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TextField, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { UIContext } from 'src/context/ui'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';

import SearchIcon from '@mui/icons-material/Search';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

interface Permission {
  _id: string
  permissionName: string
  isActive: boolean
}
const AddCentralPermission: React.FC<{ id: string }> = ({ id }) => {
  const { getData, closeData } = useContext(UIContext)
  const [pageSize, setPageSize] = useState<number>(10)
  const [total, setTotal] = useState<number>()
  const [search, setSearch] = useState<string>('')
  const [page, setPage] = useState(0)
  const [permission, setPermission] = useState<Permission[]>([])
  const [isLoading, SetIsLoading] = useState<boolean>(true)


  const [selectAll, setSelectAll] = useState(false);
  const [selectedPermission, setSelectedPermission] = useState<string[]>([]);

  useEffect(() => {
    get();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageSize, page, search]);

  useEffect(() => {
    if (getData) {
      get();
      closeData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getData, closeData]);


  useEffect(() => {
    console.log("permissiones actualizados:", selectedPermission);
  }, [selectedPermission]);

  const handleCheckboxChange = (permissionId: string) => {
    const updatedpermissiones = [...selectedPermission];

    if (updatedpermissiones.includes(permissionId)) {
      // Si el permission ya está seleccionado, quítalo
      updatedpermissiones.splice(updatedpermissiones.indexOf(permissionId), 1);
    } else {
      // Si el permission no está seleccionado, agrégalo
      updatedpermissiones.push(permissionId);
    }

    // Actualiza el estado con la nueva lista de permissiones seleccionados
    setSelectedPermission(updatedpermissiones);
  };
  const handleSelectAllChange = () => {
    // Cambia el estado de selectAll
    setSelectAll(!selectAll);

    // Si selectAll es verdadero, selecciona todos los permissiones; de lo contrario, vacía la lista de permissiones seleccionados
    if (!selectAll) {
      const permissions = permission.map((permiso) => permiso._id);
      setSelectedPermission(permissions);
      console.log("permissions", permissions);
    } else {
      setSelectedPermission([]);
      console.log("permissiones", []);
    }
  };


  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    console.log("handleChangePage ", newPage)
    setPage(newPage)
  }
  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    console.log("handleChangeRowsPerPage ", event.target.value)
    setPageSize(parseInt(event.target.value));
    setPage(0);
  };
  const get = async () => {
    try {
      SetIsLoading(true)
      console.log('limit', pageSize)
      console.log('offset', page + 1)
      console.log('search', search)
      setPermission([])
      const response = await axios.get(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}permission?limit=${pageSize}&offset=${page + 1}&name=central_${search}`
      )
      console.log("url: ", process.env.NEXT_PUBLIC_API_CENTRAL, "permission?limit=", pageSize, "&offset=", page + 1, "&search=", search)
      setPermission(response.data.data)
      setTotal(response.data.total)
      console.log(response.data)
      SetIsLoading(false);
    } catch (error: any) {
      toast.error(error.response.data.message)
      console.log(error)
    }
  }

  return (
    <Card>
      <div style={{ width: '100%' }}>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-contpermissions="panel1a-content"
            id="panel1a-header"
          >
            <Typography variant='h4' textAlign={'center'}>Central</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Grid container spacing={1}>
              <Grid item xs={6}>
                <Grid container spacing={2}>
                  <Grid item xs={2} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                    <SearchIcon />
                  </Grid>
                  <Grid item xs={10}>
                    <TextField
                      value={search}
                      onChange={(e) => {
                        setSearch(e.target.value)
                        setPage(0)
                      }}
                      style={{ margin: 20, fontFamily: 'times-new-roman' }}
                    ></TextField>
                  </Grid>
                </Grid>

              </Grid>
            </Grid>
            <Grid item xs={12}>
              <TableContainer>
                <table style={{ width: '100%', margin: 'auto' }}>
                  <TableHead>
                    <TableRow>
                      <TableCell style={{ width: '5%' }}>
                        <Checkbox
                          checked={selectAll}
                          onChange={handleSelectAllChange} />
                      </TableCell>
                      <TableCell style={{ width: "20%" }}>
                        <Typography textAlign={'center'}>
                          Opciones
                        </Typography>
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {isLoading ? (
                      <TableRow>
                        <TableCell>
                          <CircularProgress color='primary' />
                        </TableCell>
                        <TableCell>
                          <CircularProgress color='primary' />
                        </TableCell>
                        <TableCell>
                          <CircularProgress color='primary' />
                        </TableCell>
                      </TableRow>
                    ) : (
                      <>
                        {permission.map((data) => (
                          <TableRow key={data._id}
                            hover>
                            <TableCell style={{ width: '5%' }}>
                              <Checkbox
                                checked={selectAll || selectedPermission.includes(data._id)}
                                onChange={() => handleCheckboxChange(data._id)} />
                            </TableCell>
                            <TableCell style={{ width: "60%" }}>
                              <Typography >
                                {data.permissionName}
                              </Typography>
                            </TableCell>
                          </TableRow>
                        ))}
                      </>
                    )}
                  </TableBody>
                </table>
              </TableContainer>
              <Grid item xs={12}>
                <TablePagination
                  component="div"
                  count={total !== undefined ? total : 100}
                  page={page}
                  onPageChange={handleChangePage}
                  rowsPerPage={pageSize}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                />
              </Grid>
            </Grid>
          </AccordionDetails>
        </Accordion></div>
    </Card>
  )
}
export default AddCentralPermission
