import { Button, Card, Dialog, Drawer, Grid, TextField, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useState } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import { UIContext } from 'src/context/ui'
import EditIcon from '@mui/icons-material/Edit'

interface rol {
  _id: string
  rolName: string
  permissionName: string[]
}
const EditRol: React.FC<{ id: string }> = ({ id }) => {
  const { openData } = useContext(UIContext)
  const [name, setName] = useState<string>('')
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }
  const getUser = async () => {
    console.log('getUser')
    try {
      const response = await axios.get<rol>(`${process.env.NEXT_PUBLIC_API_CENTRAL}rol/${id}`)
      console.log('response', response.data)
      setName(response.data.rolName)
      console.log(response.data)
      handleOpen()
    } catch (error: any) {
      toast.error(error.response.data.message)
    }
  }
  const updateData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.put<rol>(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}rol/${id}`,
        {
          rolName: name
        },
        config
      )
      openData()
      console.log(response.data)
      handleClose()
      toast.success("Rol editado exitosamente")
    } catch (error: any) {
      toast.error(error.response.data.message)
      handleClose()
    }
  }

  return (
    <>
      {findPermission('CENTRAL_EDITAR_ROL') ? (
        <>
          <Button onClick={getUser}>
            <EditIcon style={{ color: "#77dd77" }}></EditIcon>
          </Button>
          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, md: 600, sm: 600, xl: 1000 } } }}>
            <Grid
              container
              spacing={2}
              style={{ padding: 25 }}>
              <Grid item xs={12}>
                <Typography style={{ margin: 20 }} variant='h5' textAlign={'center'}>Editar Nombre de Rol</Typography>
              </Grid>
              <Grid style={{ textAlign: 'center' }} item xs={12}>
                <TextField
                  onChange={handleNameChange}
                  label='nombre'
                  value={name}
                  style={{ width: '100%' }}>
                </TextField>
              </Grid>
              <Grid style={{ textAlign: 'left' }} item xs={12}>
                <Button
                  onClick={() => updateData()}
                  sx={{ mb: 2.5 }}
                  variant='contained'
                  style={{ marginRight: 3, marginLeft: 3, marginTop: 5, marginBottom: 5 }}>
                  Aceptar
                </Button>
                <Button
                  onClick={handleClose}
                  sx={{ mb: 2.5 }}
                  variant='contained'
                  style={{ marginRight: 3, marginLeft: 3, marginTop: 5, marginBottom: 5 }}>
                  Cancelar
                </Button>
              </Grid>
            </Grid>
          </Drawer>
          {/* <Dialog onClose={handleClose} open={open}>
            <Card style={{ height: '60%', margin: 20, borderColor: 'tomato', borderStyle: 'dotted' }}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Typography style={{ margin: 20 }} variant='h5' textAlign={'center'}>Editar Nombre de Rol</Typography>
                </Grid>
                <Grid style={{ textAlign: 'center' }} item xs={12}>
                  <TextField onChange={handleNameChange} label='nombre' value={name}></TextField>
                </Grid>
                <Grid style={{ textAlign: 'right' }} item xs={12}>
                  <Button onClick={() => updateData()}>Aceptar</Button>
                  <Button onClick={handleClose}>Cancelar</Button>
                </Grid>
              </Grid>
            </Card>
          </Dialog> */}
        </>
      ) : (
        <></>
      )
      }
    </>
  )
}
export default EditRol
