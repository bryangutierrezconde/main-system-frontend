import { Box, Button, Dialog, DialogActions, DialogContent, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useState } from 'react'
import { findPermission } from '../permisos'
import { UIContext } from 'src/context/ui'
import 'react-toastify/dist/ReactToastify.css';
import toast from 'react-hot-toast'

import AddCircleIcon from '@mui/icons-material/AddCircle';

const AddPermissions: React.FC<{ rolId: string, id: string, allId: string[] }> = ({ rolId, id, allId }) => {
  const { openData } = useContext(UIContext)
  const [userInput, setUserInput] = useState<string>('yes')
  const [open, setOpen] = useState<boolean>(false)
  const [secondDialogOpen, setSecondDialogOpen] = useState<boolean>(false)
  const handleClose = () => setOpen(false)
  const handleSecondDialogClose = () => setSecondDialogOpen(false)
  const handleConfirmation = (value: string) => {
    handleClose()
    setUserInput(value)
    setSecondDialogOpen(true)
    if (value === "yes") {
      addPermissions()
    }
  }
  const onSubmit = () => setOpen(true)
  const addPermissions = async () => {
    const token = window.localStorage.getItem('accestoken')


    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      console.log("allId", allId.length)
      if (allId.length == 0) {
        const response = await axios.put(`${process.env.NEXT_PUBLIC_API_CENTRAL}rol/set-permission-to-rol/${rolId}`,
          {
            permissionName: [id]
          }, config)
        console.log(response.data)
      }
      else {
        const response = await axios.put(`${process.env.NEXT_PUBLIC_API_CENTRAL}rol/set-permission-to-rol/${rolId}`,
          {
            permissionName: allId
          }, config)
        console.log(response.data)
      }
      openData()
      toast.success("Permiso Asignado Correctamente")
    } catch (error: any) {
      console.log(error)
      toast.error(error.response.data.message)
    }
  }

  return (
    <>
      {findPermission('CENTRAL_ELIMINAR_ROL') ? (
        <>
          <Button onClick={onSubmit}>
            <AddCircleIcon />
          </Button>
          <Dialog fullWidth maxWidth='xs' open={open} onClose={handleClose}>
            <DialogContent>
              <Box sx={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <Box sx={{ maxWidth: '85%', textAlign: 'center', '& svg': { mb: 4, color: 'warning.main' } }}>
                  {/* <Icon icon='mdi:alert-circle-outline' fontSize='5px' /> */}
                  <Typography>Esta seguro que desea asignar el permiso?</Typography>
                </Box>
              </Box>
            </DialogContent>
            <DialogActions sx={{ justifyContent: 'center' }}>
              <Button variant='contained' onClick={() => handleConfirmation('yes')}>
                Si
              </Button>
              <Button variant='outlined' color='secondary' onClick={() => handleConfirmation('cancel')}>
                Cancelar
              </Button>
            </DialogActions>
          </Dialog>
          <Dialog fullWidth maxWidth='xs' open={secondDialogOpen} onClose={handleSecondDialogClose}>
            <DialogContent>
              <Box
                sx={{
                  display: 'flex',
                  alignItems: 'center',
                  flexDirection: 'column',
                  '& svg': {
                    mb: 14,
                    color: userInput === 'yes' ? 'success.main' : 'error.main'
                  }
                }}
              >
                {/* <Icon
                  component={IconButton}
                  fontSize="medium"
                  icon={userInput === 'yes' ? 'mdi:check-circle-outline' : 'mdi:close-circle-outline'}
                /> */}
                <Typography variant='h4' sx={{ mb: 8 }}>
                  {userInput === 'yes' ? 'Asignado!' : 'Cancelado'}
                </Typography>
                <Typography>
                  {userInput === 'yes' ? 'Permiso Asignado.' : 'Asignacion de Permiso Cancelada!'}
                </Typography>
              </Box>
            </DialogContent>
            <DialogActions sx={{ justifyContent: 'center' }}>
              <Button variant='contained' color='success' onClick={handleSecondDialogClose}>
                OK
              </Button>
            </DialogActions>
          </Dialog>
        </>
      ) : (
        <></>
      )}
    </>
  )
}
export default AddPermissions
