import { Accordion, AccordionDetails, AccordionSummary, Box, Button, Checkbox, Collapse, Drawer, Grid, Paper, Tab, TableCell, TableContainer, TableRow, Tabs, Typography, useTheme } from '@mui/material'
import axios from 'axios'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import React, { useEffect, useState } from 'react'
import VisibilityIcon from '@mui/icons-material/Visibility'
import DeletePermissionRol from './deletePermissionRol';
import useMediaQuery from '@mui/material/useMediaQuery';

interface PermisionName {
  _id: string
  permissionName: string
}
interface rol {
  _id: string
  rolName: string
  aplicattion: string
  permissionName: PermisionName[]
}


interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function CustomTabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const PermisoId: React.FC<{ id: string }> = ({ id }) => {
  const [name, setName] = useState<rol>()
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }


  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };


  const GetPermiso = async () => {
    try {
      const response = await axios.get<rol>(`${process.env.NEXT_PUBLIC_API_CENTRAL}rol/${id}`)
      console.log('response', response.data)
      setName(response.data)
      console.log(response.data)
      handleOpen()
    } catch (error: any) {
      if (error.response && error.response.status === 400) {
        toast.error(error.response.data.message)
        setTimeout(() => {
          toast.dismiss()
        }, 5000)
      } else {
        toast.error('Ocurrio un error al procesar la solicitud.')
      }
    }

  }

  const CheckApp = (nameApp: string) => {
    if (name !== undefined) {
      name.permissionName.map((permissions: any) => {

        const permission = permissions.permissionName.split('_')
        console.log("permission", permission[0])
        if (permission[0] == nameApp.toUpperCase()) {
          console.log("checkApp true")

          return true
        }
      })

      return false;
    }
  }


  const FindPermission: React.FC<{ name: string, data: any }> = ({ name, data }) => {
    // const FindPermission: React.FC<{ data: any, name: string }> = ({ data, name }) => {
    const newData: any = []
    const [selectAll, setSelectAll] = useState(false);
    const [selectedPermission, setSelectedPermission] = useState<string[]>([]);
    const [checked, setChecked] = React.useState(false);
    const handleChange = () => {
      setChecked((prev) => !prev);
    };
    useEffect(() => {
      console.log("permissiones actualizados:", selectedPermission);
    }, [selectedPermission]);
    const handleCheckboxChange = (permissionId: string) => {
      const updatedpermissiones = [...selectedPermission];

      if (updatedpermissiones.includes(permissionId)) {
        updatedpermissiones.splice(updatedpermissiones.indexOf(permissionId), 1);
      } else {
        updatedpermissiones.push(permissionId);
      }
      setSelectedPermission(updatedpermissiones);
    };
    const handleSelectAllChange = () => {
      setSelectAll(!selectAll);
      if (!selectAll) {
        const permissions = newData.map((permiso: any) => permiso.id);
        setSelectedPermission(permissions);
        console.log("permissions", permissions);
      } else {
        setSelectedPermission([]);
        console.log("permissiones", []);
      }
    };
    const filter = () => {
      console.log("data", data)
      data.permissionName.map((rol: any) => {
        const permission = rol.permissionName.split('_')
        const nameFilter = permission.slice(1).join('_')
        if (permission[0] === name.toUpperCase()) {
          console.log("permission", permission[0])
          console.log("name", name.toUpperCase())
          newData.push({
            id: rol._id,
            name: nameFilter
          })
        }
      })
    }
    filter()

    return (

      <div style={{ width: '100%' }}>
        <Accordion>
          <AccordionSummary
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography variant='h6' textAlign={'center'}>{name.toUpperCase()}</Typography>
          </AccordionSummary>
          <AccordionDetails style={{ overflow: 'hidden', flexDirection: 'column', alignItems: 'center' }}>
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Button
                  onClick={handleChange}
                  sx={{ mb: 2.5 }}
                  variant='contained' style={{ float: 'left', marginTop: 30, marginLeft: 20 }}>
                  Mostrar Filtros
                </Button>
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <Collapse in={checked}>
                <Button>dwddw</Button>
              </Collapse>
            </Grid>
            <Grid item xs={12}>
              <TableContainer component={Paper}>
                <TableRow>
                  <TableCell style={{ width: '5' }} >
                    <Checkbox
                      checked={selectAll}
                      onChange={handleSelectAllChange} />
                  </TableCell>
                  <TableCell style={{ width: '25' }}>
                    <Typography
                      variant='body2' textAlign={'center'} >Eliminar</Typography>
                  </TableCell>
                  <TableCell style={{ width: '70' }}>
                    <Typography
                      variant='body2' textAlign={'center'}>
                      Nombre
                    </Typography>
                  </TableCell>
                </TableRow>
                {
                  newData.map((name: any) => (
                    <TableRow key={name.id}
                      hover>
                      <TableCell style={{ width: '5' }} >
                        <Checkbox
                          checked={selectAll || selectedPermission.includes(name.id)}
                          onChange={() => {
                            handleCheckboxChange(name.id)
                            console.log("name", name)
                          }} />
                      </TableCell>
                      <TableCell style={{ width: '25' }} >
                        <DeletePermissionRol id={id} permissionId={name.id} allPermissions={selectedPermission} />
                      </TableCell>
                      <TableCell style={{ width: '70' }}>
                        <Typography
                          variant='subtitle2'>
                          {name.name}
                        </Typography>
                      </TableCell>
                    </TableRow>
                  ))
                }
              </TableContainer>
            </Grid>
          </AccordionDetails>
        </Accordion></div>
    )
  }

  return (
    <>
      {findPermission('CENTRAL_EDITAR_ROL') ? (
        <>
          <Button onClick={GetPermiso}>
            <VisibilityIcon style={{ color: '#77dd77' }}></VisibilityIcon>
          </Button>

          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, md: 600, sm: 600, xl: 1000 } } }}
          >
            {name?.permissionName.length != 0 ? (
              <Grid style={{ padding: 20 }} container spacing={2}>
                <Grid item xs={12}
                  style={{ marginBottom: 20 }}>
                  <Typography variant='h6' textAlign={'left'} >
                    permisos de {name?.rolName.toLowerCase()}
                  </Typography>
                </Grid>
                {name !== undefined ? (

                  <>
                    <FindPermission name={'central'} data={name} />
                    <FindPermission name={'personal'} data={name} />
                    <FindPermission name={'activo'} data={name} />
                    <FindPermission name={'biblioteca'} data={name} />
                    <FindPermission name={'gestiondocumental'} data={name} />
                  </>

                  // <Box sx={{ width: '100%' }}>
                  //   <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                  //     <Tabs value={value} onChange={handleChange} aria-label="basic tabs example">
                  //       {CheckApp('central') ? (<Tab label="Central" {...a11yProps(0)} />) : (<></>)}
                  //       {CheckApp('personal') ? (<Tab label="Personal" {...a11yProps(1)} />) : (<></>)}
                  //       {CheckApp('activo') ? (<Tab label="Activos" {...a11yProps(2)} />) : (<></>)}
                  //       {CheckApp('biblioteca') ? (<Tab label="Biblioteca" {...a11yProps(2)} />) : (<></>)}
                  //       {CheckApp('gestiondocumental') ? (<Tab label="Gestion Documental" {...a11yProps(2)} />) : (<></>)}
                  //     </Tabs>
                  //   </Box>
                  //   <CustomTabPanel value={value} index={0}>
                  //     <FindPermission name={'central'} data={name} />
                  //   </CustomTabPanel>
                  //   <CustomTabPanel value={value} index={1}>
                  //     <FindPermission name={'personal'} data={name} />
                  //   </CustomTabPanel>
                  //   <CustomTabPanel value={value} index={2}>
                  //     <FindPermission name={'activo'} data={name} />
                  //   </CustomTabPanel>
                  //   <CustomTabPanel value={value} index={3}>
                  //     <FindPermission name={'biblioteca'} data={name} />
                  //   </CustomTabPanel>
                  //   <CustomTabPanel value={value} index={4}>
                  //     <FindPermission name={'gestiondocumental'} data={name} />
                  //   </CustomTabPanel>
                  // </Box>



                ) : (
                  <></>
                )}
                <Grid item xs={12}>
                  <Button
                    onClick={handleClose}
                    sx={{ mb: 2.5 }}
                    variant='contained'
                    style={{ marginTop: 15 }}>
                    Cerrar
                  </Button>
                </Grid>
              </Grid>
            ) : (
              <Grid
                container
                spacing={2}
                style={{ padding: 20 }}
              >
                <Grid item xs={12}>
                  <Typography
                    style={{ borderRadius: 15, padding: 15, textAlign: 'left', margin: 20 }}
                    variant='h6'
                  >
                    Este Rol no tiene ningun permiso Asignado
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Button
                    onClick={handleClose}
                    sx={{ mb: 2.5 }}
                    variant='contained'
                    style={{ marginTop: 15 }}>
                    Aceptar
                  </Button>
                </Grid>
              </Grid>
            )}
          </Drawer>
        </>
      ) : (
        <></>
      )
      }
    </>
  )
}
export default PermisoId
