import { Box, Button, Drawer, Grid, MenuItem, Select, SelectChangeEvent, TextField, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useState } from 'react'
import toast from 'react-hot-toast'
import 'react-toastify/dist/ReactToastify.css';
import { UIContext } from 'src/context/ui'
import { findPermission } from '../permisos'
import AddIcon from '@mui/icons-material/Add'

const NewRol = () => {
  const { openData } = useContext(UIContext)
  const [name, setName] = useState<string>()
  const [appName, setAppName] = useState<string>("central")
  const [open, setOpen] = useState<boolean>(false)
  const [app, setApp] = useState<string>('10')
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }
  const handleAppChange = (event: SelectChangeEvent) => {
    const selected = event.target.value as string
    setApp(selected);
    console.log(selected)
    if (selected == "10") {
      setAppName("central")
      console.log("central")
    }
    if (selected == "20") {
      setAppName("personal")
      console.log("personal")
    }
    if (selected == "30") {
      setAppName("activo")
      console.log("activo")
    }
    if (selected == "40") {
      setAppName("biblioteca")
      console.log("biblioteca")
    }
    if (selected == "50") {
      setAppName("gestion documental")
      console.log("gestion documental")
    }
  };
  const postData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      handleClose()
      console.log(config)
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}rol`,
        {
          rolName: name,
          aplicattion: appName
        },
        config
      )
      console.log(response.data)

      openData()
      setName('')
      toast.success("Rol creado exitosamente")
    } catch (error: any) {
      handleClose()
      toast.error(error.response.data.message)
    }
  }

  return (
    <>
      {findPermission('CENTRAL_CREAR_ROL') ? (
        <>
          <Button sx={{ mb: 2.5 }} variant='contained' style={{ float: 'right', marginTop: 30, marginRight: 20 }} onClick={handleOpen}>
            Nuevo Rol <AddIcon></AddIcon>
          </Button>
          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, md: 600, sm: 600, xl: 1000 } } }}
          >
            <Box sx={{ p: 5 }}>
              <Grid container spacing={2} style={{ padding: 25 }}>
                <Grid item xs={12}>
                  <Typography variant='h6' textAlign={'left'}>
                    Nuevo Rol
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant='subtitle2'>
                    Coloque el nombre para el nuevo rol, no use espacios ni caracteres especiales, tampoco numeros
                  </Typography>
                </Grid>
                <Grid style={{ textAlign: 'center', marginBottom: 10 }} item xs={12} md={6} sm={6} lg={6}>
                  <Typography style={{ textAlign: 'left', marginBottom: 15, marginTop: 15 }} variant='body2'>Aplicaciones</Typography>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={app}
                    label="Aplicacion"
                    onChange={handleAppChange}
                    style={{ width: '100%' }}
                  >
                    <MenuItem value={10}>Central</MenuItem>
                    <MenuItem value={20}>Personal</MenuItem>
                    <MenuItem value={30}>Activos</MenuItem>
                    <MenuItem value={40}>Biblioteca</MenuItem>
                    <MenuItem value={50}>Gestion Documental</MenuItem>
                  </Select>
                </Grid>
                <Grid style={{
                  textAlign: 'center', width: '50%'
                }} item xs={12} md={6} sm={6} lg={6}>
                  < Typography style={{ textAlign: 'left', marginBottom: 15, marginTop: 15 }} variant='body2' >Nombre del Rol</Typography>
                  <TextField
                    label='Nombre'
                    value={name}
                    onChange={handleChange}
                    style={{ width: '100%' }}></TextField>
                </Grid>
              </Grid>
              <Grid style={{ textAlign: 'left', marginTop: 15, marginLeft: 25 }} item xs={12}>
                <Button
                  onClick={() => postData()}
                  sx={{ mb: 2.5 }}
                  variant='contained'
                  style={{ marginLeft: 3, marginRight: 3, marginTop: 5, marginBottom: 5 }}>
                  Aceptar
                </Button>
                <Button
                  onClick={handleClose}
                  sx={{ mb: 2.5 }}
                  variant='contained'
                  style={{ marginLeft: 3, marginRight: 3, marginTop: 5, marginBottom: 5 }}>
                  Cancelar
                </Button>
              </Grid>
            </Box>
          </Drawer>
        </>
      ) : (
        <></>
      )
      }
    </>
  )
}
export default NewRol
