import { Button, Card, Dialog, Drawer, Grid, TableCell, TableRow, Typography } from '@mui/material'
import axios from 'axios'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import React, { useEffect, useState } from 'react'
import CancelPresentationIcon from '@mui/icons-material/CancelPresentation'
import VisibilityIcon from '@mui/icons-material/Visibility'
import DeleteRolUser from './deleteRolUser';

interface PermisionName {
  _id: string
  permissionName: string
}
interface rol {
  _id: string
  rolName: string
  permissionName: PermisionName[]
}
interface user {
  id: string
  roles: string[]
}
const RolId: React.FC<{ id: string }> = ({ id }) => {
  const [user, setUser] = useState<user>()
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const GetUser = async () => {
    try {
      const response = await axios.get<user>(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/${id}`)
      console.log('response', response.data)
      setUser(response.data)
      console.log(response.data)
      handleOpen()
    } catch (error: any) {
      if (error.response && error.response.status === 400) {
        toast.error(error.response.data.message)
        setTimeout(() => {
          toast.dismiss()
        }, 5000)
      } else {
        toast.error('Ocurrio un error al procesar la solicitud.')
      }
    }
  }

  return (
    <>
      {findPermission('CENTRAL_EDITAR_ROL') ? (
        <>
          <Button onClick={GetUser}>
            <VisibilityIcon style={{ color: 'Aquamarine' }}></VisibilityIcon>
          </Button>
          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 600, md: 600, xl: 1000 } } }}
          >
            <Grid
              container
              spacing={2}
              justifyContent="center"
              alignItems="center"
              style={{ width: '100%' }}>
              <Grid item xs={12}>
                <Typography variant='h4' textAlign={'center'} >
                  Roles
                </Typography>
              </Grid>
              <Grid
                item
                xs={12}
              >
                {/* <Card style={{
                  marginLeft: '10%',
                  width: '80%',
                  textAlign: 'center'
                }}>
                  <TableRow>
                    <TableCell style={{ width: '50%' }}>
                      <Typography>
                        Opciones
                      </Typography>
                    </TableCell>
                    <TableCell style={{ width: '50%' }}>
                      <Typography>
                        Nombre
                      </Typography>
                    </TableCell>
                  </TableRow>
                  {user !== undefined ? (
                    <>
                      {user.roles.map((rol, index) => (
                        <GetRolName idUser={id} id={rol} key={index} />
                      ))}
                    </>
                  ) : (
                    <></>
                  )}
                </Card> */}

                <Card style={{ marginLeft: '10%', width: '80%' }}>
                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <Typography
                        variant='body2'
                        textAlign={'center'}
                        style={{ marginTop: 10 }}>Opciones</Typography>
                    </Grid>
                    <Grid item xs={6}>
                      <Typography
                        variant='body2'
                        textAlign={'center'}
                        style={{ marginTop: 10 }}>Nombre</Typography>
                    </Grid>
                    {user?.roles.map((rol, index) => (
                      <GetRolName idUser={id} id={rol} key={index} />
                    ))}
                  </Grid>
                </Card>
              </Grid>
            </Grid>
          </Drawer>
        </>
      ) : (
        <></>
      )}
    </>
  )
}

const GetRolName: React.FC<{ idUser: string, id: string }> = ({ idUser, id }) => {
  const [rol, setRol] = useState<rol>()

  useEffect(() => {
    GetName()
  }, [])
  const GetName = async () => {
    try {
      const res = await axios.get<rol>(`${process.env.NEXT_PUBLIC_API_CENTRAL}rol/${id}`)
      console.log('res rolname', rol)
      setRol(res.data)
    } catch (error: any) {
      console.log('error ', error)
    }
  }

  return (

    // <TableRow style={{ width: '100%' }}>
    //   <TableCell style={{ width: '50%' }}>
    //     <DeleteRolUser idUser={idUser} rolId={rol?._id} />
    //   </TableCell>
    //   <TableCell style={{ width: '50%' }}>
    //     <Typography
    //       variant='subtitle1'>
    //       {rol?.rolName}
    //     </Typography>
    //   </TableCell>

    // </TableRow>

    <>
      <Grid item xs={6}>
        <DeleteRolUser idUser={idUser} rolId={rol?._id} />
      </Grid>
      <Grid item xs={6}>
        <Typography
          variant='subtitle1'
          textAlign={'center'}>
          {rol?.rolName}
        </Typography>
      </Grid>
    </>
  )
}
export default RolId
