// components/LoadingSpinner.js

import React from 'react';
import styled, { keyframes } from 'styled-components';

const spin = keyframes`
  0%, 100% {
    transform: rotateY(0deg);
    animation-timing-function: cubic-bezier(0.5, 0, 1, 0.5);
  }
  0% {
    transform: rotateY(0deg);
  }
  50% {
    transform: rotateY(1800deg);
    animation-timing-function: cubic-bezier(0, 0.5, 0.5, 1);
  }
  100% {
    transform: rotateY(3600deg);
  }
`;

const CenteredContainer = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 9999; /* Valor alto para estar por encima de todo */
`;

const Circle = styled.div`
  display: inline-block;
  width: 128px;
  height: 128px;
  margin: 16px;
  border-radius: 50%;
  background: red;
  animation: ${spin} 3s cubic-bezier(0, 0.2, 0.8, 1) infinite; /* Duración ajustada a 3 segundos */
`;

const LoadingSpinner = () => {
  return (
    <CenteredContainer>
      <Circle />
    </CenteredContainer>
  );
};

export default LoadingSpinner;
