import { Button, Drawer, Grid, Tooltip, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useState } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import { UIContext } from 'src/context/ui'
import CancelPresentationIcon from '@mui/icons-material/CancelPresentation'

interface PermissionName {
  _id: string
  permissionName: string
}
interface Rol {
  _id: string
  rolName: string
  permissionName: PermissionName
}
const AddRol: React.FC<{ id: string }> = ({ id }) => {
  const [rol, setRol] = useState<Rol[]>([])
  const [open, setOpen] = useState(false)
  const { openData } = useContext(UIContext)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  const GetRol = async () => {
    try {
      const response = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}rol?limit=50`)
      console.log('response', response.data.data)
      setRol(response.data.data)
      handleOpen()

    } catch (error: any) {
      if (error.response && error.response.status === 400) {
        toast.error(error.response.data.message)
        setTimeout(() => {
          toast.dismiss()
        }, 5000)
      } else {
        toast.error('Ocurrio un error al procesar la solicitud.')
      }
    }
  }
  const setRolName = async (name: string) => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.put(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}user/set-rol-to-user/${id}`,
        {
          rolName: name
        },
        config
      )
      console.log(response.data)
      openData()
      handleClose()
      toast.success('Rol asignado correctamente')
    } catch (error: any) {
      handleClose()
      toast.error(error.response.data.message)
    }
  }

  return (
    <>
      {findPermission('CENTRAL_ESTABLECER_ROL_USUARIO') ? (
        <>
          <Button onClick={GetRol}>
            <Typography variant='subtitle1'>
              Asignar Rol
            </Typography>
          </Button>
          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 200, sm: 400, md: 600, xl: 1000 } } }}
          >
            <Grid container spacing={2} style={{ padding: 20 }}>
              <Grid item xs={12}>
                <Typography variant='h6' textAlign={'left'}>
                  Roles
                </Typography>
              </Grid>
              {rol.map(rol => (
                <Grid
                  item
                  xs={6}
                  key={rol._id}>
                  <Button
                    sx={{ mb: 2.5 }}
                    variant='contained'
                    style={{
                      float: 'left',
                      marginTop: 30,
                      marginLeft: 20,
                      width: '95%',
                      height: '90%'
                    }}
                    onClick={() => setRolName(rol.rolName)}
                  >
                    <Tooltip title={rol.rolName || ''}>
                      <div style={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>
                        <Typography noWrap variant='body2'>
                          {rol.rolName}
                        </Typography>
                      </div>
                    </Tooltip>
                  </Button>
                </Grid>
              ))}
              <Grid item xs={12}>
                <Button
                  onClick={handleClose}
                  sx={{ mb: 2.5 }}
                  variant='contained'
                  style={{
                    marginTop: 30,
                    marginLeft: 20
                  }}>
                  Cerrar
                </Button>
              </Grid>
            </Grid>
          </Drawer>
        </>
      ) : (
        <></>
      )
      }
    </>
  )
}
export default AddRol
