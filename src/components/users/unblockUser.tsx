import { Button, TableCell, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext } from 'react'
import { findPermission } from '../permisos'
import { UIContext } from 'src/context/ui'
import 'react-toastify/dist/ReactToastify.css';
import toast from 'react-hot-toast'

const UnblockUser: React.FC<{ idUser: string }> = ({ idUser }) => {
  const { openData } = useContext(UIContext)
  const deleteData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.post(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/unblock-user/${idUser}`, {
      }, config)
      console.log(response.data)
      openData()
      toast.success("Usuario Bloqueado Exitosamente")
    } catch (error: any) {
      console.log(error)
      toast.error(error.response.data.message)
    }
  }

  return (
    <>
      {findPermission('CENTRAL_ELIMINAR_ROL') ? (
        <div style={{ width: '100%' }}>
          <Button onClick={deleteData}>
            <Typography fontSize={15} textAlign={'center'} style={{ backgroundColor: '#77dd77', borderRadius: 20, paddingLeft: '20%', paddingRight: '20%' }}>
              Desbloquear
            </Typography>
          </Button>
        </div>
      ) : (
        <></>
      )}
    </>
  )
}
export default UnblockUser
