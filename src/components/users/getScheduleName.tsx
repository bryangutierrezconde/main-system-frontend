import { Typography } from "@mui/material"
import axios from "axios"
import React, { useEffect, useState } from "react";
import { toast } from "react-toastify"
import 'react-toastify/dist/ReactToastify.css';

const GetScheduleName: React.FC<{ id: string }> = ({ id }) => {
  const [name, setName] = useState<string>("")
  useEffect(() => {
    get()
  })
  const get = async () => {
    try {
      const response = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/user-schedule/${id}`)
      setName(response.data.name)
      console.log("response", response.data)
    }
    catch (error: any) {
      toast.error(error.response.data.message)
      console.log(error)
    }
  }

  return (
    <Typography>
      {name}
    </Typography>
  )
}
export default GetScheduleName
