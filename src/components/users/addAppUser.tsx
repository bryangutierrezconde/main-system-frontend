import { Button, Dialog, Drawer, Grid, Typography } from '@mui/material'
import axios from 'axios'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import { useContext, useState } from 'react'
import CancelPresentationIcon from '@mui/icons-material/CancelPresentation'
import { UIContext } from 'src/context/ui'

interface App {
  _id: string
  name: string
  expiresIn: string
  isDeleted: boolean
  uuid: string
}
const AddAppUser: React.FC<{ id: string }> = ({ id }) => {
  const { openData } = useContext(UIContext)
  const [app, setApp] = useState<App[]>([])
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const GetApp = async () => {
    try {
      const response = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}apps?limit=50`)
      console.log('response', response.data.data)
      setApp(response.data.data)
      console.log(response.data)
      handleOpen()
    } catch (error: any) {
      if (error.response && error.response.status === 400) {
        toast.error(error.response.data.message)
        setTimeout(() => {
          toast.dismiss()
        }, 5000)
      } else {
        toast.error('Ocurrio un error al procesar la solicitud.')
      }
    }
  }
  const SetApp = async (name: string) => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      console.log('app :', name)
      const response = await axios.put(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}user/set-app-to-user/${id}`,
        {
          name: name
        },
        config
      )
      console.log('response :', response.data)
      handleClose()
      openData()
    } catch (error: any) {
      toast.error(error.response.data.message)
      handleClose()
    }
  }

  return (
    <>
      {findPermission('CENTRAL_ESTABLECER_APP_USUARIO') ? (
        <>
          <Button onClick={GetApp}>
            <Typography variant='subtitle1'>
              Asignar
            </Typography>
          </Button>
          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 600, md: 600, xl: 1000 } } }}
          >
            <Grid container spacing={2} style={{ padding: 20 }}>
              <Grid item xs={12}>
                <Typography style={{ marginLeft: 20 }} variant='h6' textAlign={'left'} >
                  Aplicacion
                </Typography>
              </Grid>
              {app.map(app => (
                <Grid
                  item
                  xs={6}
                  key={app._id}>
                  <Button
                    onClick={() => SetApp(app.name)}
                    sx={{ mb: 2.5 }}
                    variant='contained'
                    style={{
                      float: 'left',
                      marginTop: 30,
                      marginLeft: 20,
                      width: '95%',
                      height: '90%'
                    }}>
                    {/* <Typography textAlign={'center'}>{app.name}</Typography> */}
                    {app.name}
                  </Button>
                </Grid>
              ))}
              <Grid item xs={12}>
                <Button
                  onClick={handleClose}
                  sx={{ mb: 2.5 }}
                  variant='contained'
                  style={{
                    marginTop: 30,
                    marginLeft: 20
                  }}>
                  Cerrar
                </Button>
              </Grid>
            </Grid>

          </Drawer>
        </>
      ) : (
        <></>
      )}
    </>
  )
}
export default AddAppUser
