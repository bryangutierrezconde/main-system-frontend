import authConfig from 'src/configs/auth'
import { useRouter } from 'next/router'

export const Logout = () => {
  const router = useRouter()
  localStorage.removeItem('refreshToken')
  localStorage.removeItem(authConfig.storageTokenKeyName)
  window.localStorage.removeItem('id')
  window.localStorage.removeItem('central')
  window.localStorage.removeItem('personal')
  window.localStorage.removeItem('activo')
  window.localStorage.removeItem('biblioteca')
  window.localStorage.removeItem('gestion-documental')
  window.localStorage.removeItem('token-central')
  window.localStorage.removeItem('token-personal')
  window.localStorage.removeItem('token-activo')
  window.localStorage.removeItem('token-biblioteca')
  window.localStorage.removeItem('token-gestion-documental')
  window.localStorage.removeItem('systems')
  window.localStorage.removeItem('permisos')
  router.push('/login')
}
