import { Button, Dialog, TextField } from '@mui/material'
import axios from 'axios'
import React, { useContext, useState } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import { UIContext } from 'src/context/ui'

const AddApp: React.FC<{ id: string }> = ({ id }) => {
  const [app, setApp] = useState<string>()
  const [open, setOpen] = useState(false)
  const { openData } = useContext(UIContext)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const handleAppChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setApp(e.target.value)
  }
  const putData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.put(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}user/set-app-to-user/${id}`,
        {
          name: app
        },
        config
      )
      console.log(response.data)
      setApp('')
      openData()
      handleClose()
    } catch (error: any) {
      handleClose()
      toast.error(error.response.data.message)
    }
  }

  return (
    <>
      {findPermission('CENTRAL_ESTABLECER_APP_USUARIO') ? (
        <>
          <Button onClick={handleOpen}>Asignar App</Button>
          <Dialog onClose={handleClose} open={open}>
            <TextField value={app} onChange={handleAppChange} required label='Rol'></TextField>
            <Button onClick={putData}>Aceptar</Button>
            <Button onClick={handleClose}>Cancelar</Button>
          </Dialog>
        </>
      ) : (
        <></>
      )}
    </>
  )
}
export default AddApp
