/* eslint-disable @typescript-eslint/no-var-requires */
import { Button, Drawer, Grid, TextField, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useState } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import { UIContext } from 'src/context/ui'

const AddPassword: React.FC<{ id: string, email: string, isAssigned: boolean }> = ({ id, email, isAssigned }) => {
  const [password, setPassword] = useState<string>()
  const [open, setOpen] = useState(false)
  const { openData } = useContext(UIContext)



  const handleOpen = () => {
    setOpen(true)
    generateRandomPassword()
  }
  const handleClose = () => {
    setOpen(false)
  }

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value)
  }

  const generateRandomPassword = () => {
    // Aquí generamos la contraseña aleatoria, puedes personalizar esto según tus requisitos.
    // const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*'
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    const passwordLength = 12 // Longitud de la contraseña
    let randomPassword = ''

    for (let i = 0; i < passwordLength; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length)
      randomPassword += characters.charAt(randomIndex)
    }

    setPassword(randomPassword)
  }
  const putData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.put(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}user/set-password-to-user/${id}`,
        {
          password: password
        },
        config
      )
      handleClose()
      openData()
      console.log(response.data)
      toast.success("contraseña asignada correctamente")
      try {
        const res = await axios.post(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/send-email-password-to-user/`,
          {
            to: email,
            subject: "Contraseña asignada",
            text: password
          })
        console.log("res", res.data)
        toast.success("Contraseña enviada a la direccion de correo")
      } catch (error: any) {
        toast.error(error.response.data.message)
      }
    } catch (error: any) {
      toast.error(error.response.data.message)
    }
  }

  return (
    <>
      {findPermission('CENTRAL_ESTABLECER_APP_USUARIO') ? (
        <>
          {isAssigned ? (
            <Button
              onClick={handleOpen}
              style={{
                backgroundColor: '#77dd77',

                // float: 'right',
                display: 'block',
                margin: 'auto'
              }}>
              <Typography color={'white'}>
                Asignada
              </Typography>
            </Button>
          ) : (
            <Button
              onClick={handleOpen}
              style={{
                backgroundColor: 'tomato',

                // float: 'right',
                display: 'block',
                margin: 'auto'
              }}>
              <Typography color={'white'}>
                Asignar
              </Typography>
            </Button>
          )}

          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 600, md: 600, xl: 1000 } } }}
          >

            <Grid container spacing={2} style={{ padding: 25 }}>
              <Grid item xs={12}>
                <Typography variant='h6' textAlign={'left'}>
                  Contraseña
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography variant='subtitle2'>
                  Anote y guarde muy bien la contraseña que se le esta asignando, en caso de olvidar la contraseña se
                  debera solicitar una nueva contraseña, la contraseña usa diferentes tipos de caracteres para mayor
                  seguridad y no se guarda en ningun sitio, solo se guarda el token para hacer la validacion de login,
                  si esta deacuerdo con la nueva contraseña presione aceptar
                </Typography>
              </Grid>
              <Grid item xs={12} style={{ textAlign: 'center' }}>
                <TextField
                  style={{ width: '80%', marginTop: 15 }}
                  onChange={handlePasswordChange}
                  value={password}></TextField>
              </Grid>
            </Grid>
            <Grid style={{ textAlign: 'left', marginTop: 15 }} item xs={9}>
              <Button
                onClick={() => putData()}
                sx={{ mb: 2.5 }}
                variant='contained'
                style={{ marginLeft: 10 }}>
                Aceptar
              </Button>
              <Button
                onClick={() => generateRandomPassword()}
                sx={{ mb: 2.5 }}
                variant='contained'
                style={{ marginLeft: 10 }}>
                Cambiar
              </Button>
              <Button
                onClick={handleClose}
                sx={{ mb: 2.5, backgroundColor: 'transparent', color: 'red' }}
                variant='contained'
                style={{ marginLeft: 10 }}>
                Cancelar
              </Button>

            </Grid>
          </Drawer>
        </>
      ) : (
        <></>
      )
      }
    </>
  )
}
export default AddPassword
