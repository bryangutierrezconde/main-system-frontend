import { Button, Card, Dialog, Drawer, Grid, TextField, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useState } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import { UIContext } from 'src/context/ui'
import EditIcon from '@mui/icons-material/Edit'

interface apps {
  _id: string
  name: string
  isDeleted: boolean
  uuid: string
}
const EditApp: React.FC<{ id: string }> = ({ id }) => {
  const { openData } = useContext(UIContext)
  const [name, setName] = useState<string>('')
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }
  const getApp = async () => {
    console.log('getUser')
    try {
      const response = await axios.get<apps>(`${process.env.NEXT_PUBLIC_API_CENTRAL}apps/${id}`)
      console.log('response', response.data)
      setName(response.data.name)
      console.log(response.data)
      handleOpen()
    } catch (error) {
      console.log(error)
    }
  }
  const updateData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.put<apps>(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}apps/update-app${id}`,
        {
          name: name
        },
        config
      )
      console.log(response.data)
      handleClose()
      openData()
      toast.success("Aplicacion Editada exitosamente")
    } catch (error: any) {
      toast.error(error.response.data.message)
      handleClose()
    }
  }

  return (
    <>
      {findPermission('CENTRAL_EDITAR_APP') ? (
        <>
          <Button onClick={getApp}>
            <EditIcon style={{ color: "#77dd77" }}></EditIcon>
          </Button>
          <Drawer

            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, md: 600, sm: 600, xl: 1000 } } }}
          >
            <Grid
              container
              spacing={2}
              style={{ padding: '25' }}>
              <Grid item xs={12}>
                <Typography style={{ margin: 20 }} variant='h6' textAlign={'left'}>Editar Aplicacion</Typography>
              </Grid>
              <Grid style={{ textAlign: 'center', marginLeft: 20, marginRight: 20 }} item xs={12}>
                <TextField
                  onChange={handleNameChange}
                  label='Nombre'
                  value={name}
                  style={{ width: '100%' }}></TextField>
              </Grid>
            </Grid>
            <Grid style={{ textAlign: 'left', marginTop: 15, marginLeft: 20 }} item xs={12}>
              <Button
                onClick={() => updateData()}
                sx={{ mb: 2.5 }}
                variant='contained'
                style={{ marginRight: 10 }}>
                Aceptar
              </Button>
              <Button
                onClick={handleClose}
                sx={{ mb: 2.5 }}
                variant='contained'
                style={{ marginLeft: 10 }}>
                Cancelar
              </Button>
            </Grid>
          </Drawer>
          {/* <Dialog onClose={handleClose} open={open}>
            <Card style={{ height: '60%', margin: 20, borderColor: 'tomato', borderStyle: 'dotted' }}>
              <Grid container spacing={2}>
                <Grid item xs={12}>
                  <Typography style={{ margin: 20 }} variant='h5' textAlign={'center'}>Editar Aplicacion</Typography>
                </Grid>
                <Grid style={{ textAlign: 'center' }} item xs={12}>
                  <TextField onChange={handleNameChange} label='nombre' value={name}></TextField>
                </Grid>
              </Grid>
              <Grid style={{ textAlign: 'right' }} item xs={12}>
                <Button onClick={() => updateData()}>Aceptar</Button>
                <Button onClick={handleClose}>Cancelar</Button>
              </Grid>
            </Card>
          </Dialog> */}
        </>
      ) : (
        <></>
      )}
    </>
  )
}
export default EditApp
