import { Button, Card, Checkbox, CircularProgress, Grid, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, TextField, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { UIContext } from 'src/context/ui'
import EditApp from './editapp'
import DeleteApp from './deleteApp'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import NewApp from './newApp'

import SearchIcon from '@mui/icons-material/Search';
import AppGetRol from './getRolApp'
import { ThemeColor } from 'src/@core/layouts/types'
import CustomChip from 'src/@core/components/mui/chip'

interface apps {
  _id: string
  name: string
  isDeleted: boolean
  uuid: string
}

interface UserStatusType {
  [key: string]: ThemeColor
}
const userStatusObj: UserStatusType = {
  activo: 'success',
  pending: 'warning',
  inactivo: 'error'
}
const GetAllApp = () => {
  const { getData, closeData } = useContext(UIContext)
  const [pageSize, setPageSize] = useState<number>(5)
  const [total, setTotal] = useState<number>()
  const [search, setSearch] = useState<string>('')
  const [page, setPage] = useState(0)
  const [app, setApp] = useState<apps[]>([])
  const [isLoading, SetIsLoading] = useState<boolean>(true)


  const [selectAll, setSelectAll] = useState(false);
  const [selectedRoles, setSelectedRoles] = useState<string[]>([]);

  useEffect(() => {
    get();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageSize, page, search]);

  useEffect(() => {
    if (getData) {
      get();
      closeData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getData, closeData]);


  useEffect(() => {
    console.log("roles actualizados:", selectedRoles);
  }, [selectedRoles]);

  const handleCheckboxChange = (roleId: string) => {
    const updatedRoles = [...selectedRoles];

    if (updatedRoles.includes(roleId)) {
      // Si el rol ya está seleccionado, quítalo
      updatedRoles.splice(updatedRoles.indexOf(roleId), 1);
    } else {
      // Si el rol no está seleccionado, agrégalo
      updatedRoles.push(roleId);
    }

    // Actualiza el estado con la nueva lista de roles seleccionados
    setSelectedRoles(updatedRoles);
  };
  const handleSelectAllChange = () => {
    // Cambia el estado de selectAll
    setSelectAll(!selectAll);

    // Si selectAll es verdadero, selecciona todos los roles; de lo contrario, vacía la lista de roles seleccionados
    if (!selectAll) {
      const roles = app.map((rol) => rol._id);
      setSelectedRoles(roles);
      console.log("roles", roles);
    } else {
      setSelectedRoles([]);
      console.log("roles", []);
    }
  };

  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    console.log("handleChangePage ", newPage)
    setPage(newPage)
  }
  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    console.log("handleChangeRowsPerPage ", event.target.value)
    setPageSize(parseInt(event.target.value));
    setPage(0);
  };
  const get = async () => {
    try {
      SetIsLoading(true)
      console.log('limit', pageSize)
      console.log('offset', page + 1)
      console.log('search', search)
      setApp([])
      const response = await axios.get(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}apps?limit=${pageSize}&offset=${page + 1}&name=${search}`
      )
      console.log("url: ", process.env.NEXT_PUBLIC_API_CENTRAL, "apps ? limit = ", pageSize, " & offset=", page + 1, " & search=", search)
      setApp(response.data.data)
      setTotal(response.data.total)
      console.log("data", response.data.data)
      console.log('total', response.data.total)
      SetIsLoading(false);
    } catch (error: any) {
      toast.error(error.response.data.message)
      console.log('error: ', error)
    }
  }

  // if (getData) {
  //   get()
  //   closeData()
  // }

  return (
    <Card >
      <Grid container spacing={1}>
        {/* <Grid item xs={6}>
          <Typography variant='h4' style={{ margin: 20, }}>Aplicaciones</Typography>
        </Grid> */}
        {/* <Grid item xs={3}>
          <Grid container spacing={2}>
            <Grid item xs={2} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
              <SearchIcon />
            </Grid>
            <Grid item xs={10}>
              <TextField
                value={search}
                onChange={(e) => {
                  setSearch(e.target.value)
                  setPage(0)
                }}
                style={{ margin: 20 }}
              ></TextField>
            </Grid>
          </Grid>

        </Grid> */}
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <Button sx={{ mb: 2.5 }} variant='contained' style={{ float: 'left', marginTop: 30, marginLeft: 20 }}>
            Mostrar Filtros
          </Button>
        </Grid>
        <Grid item xs={12} md={6} sm={6} lg={6}>
          <NewApp />
        </Grid>
      </Grid>
      <Grid item xs={12}>
        <TableContainer>
          <table style={{ width: '100%', margin: 'auto' }}>
            <TableHead>
              <TableRow>
                <TableCell style={{ width: '5%' }}>
                  <Checkbox
                    checked={selectAll}
                    onChange={handleSelectAllChange} />
                </TableCell>
                <TableCell style={{ width: "20%", textAlign: 'center' }}>
                  <Typography variant='body2'>
                    Opciones
                  </Typography>
                </TableCell>
                <TableCell style={{ width: "15%" }}>
                  <Typography variant='body2' style={{ textAlign: 'center' }} >
                    Estado
                  </Typography>
                </TableCell>
                <TableCell style={{ width: "35%" }}>
                  <Typography variant='body2' >
                    Nombre
                  </Typography>
                </TableCell>
                <TableCell style={{ width: "15%" }}>
                  <Typography variant='body2' style={{ textAlign: 'center' }}>
                    Roles
                  </Typography>
                </TableCell>

              </TableRow>
            </TableHead>
            {isLoading ? (
              <TableBody>
                <TableRow>
                  <TableCell colSpan={12} align="center">
                    <CircularProgress color='primary' />
                  </TableCell>
                </TableRow>
              </TableBody>) : (
              <TableBody>
                {app.map((data) => (
                  <TableRow key={data._id}
                    hover>
                    <TableCell style={{ width: '5%' }}>
                      <Checkbox
                        checked={selectAll || selectedRoles.includes(data._id)}
                        onChange={() => handleCheckboxChange(data._id)} />
                    </TableCell>
                    <TableCell style={{ width: "20%", textAlign: 'center' }}>
                      <EditApp id={data._id} />
                      <DeleteApp id={data._id} allId={selectedRoles} />
                    </TableCell>
                    <TableCell style={{ width: "15%", textAlign: 'center' }}>
                      <CustomChip
                        skin='light'
                        size='small'
                        label={data.isDeleted ? 'inactivo' : 'activo'}
                        color={userStatusObj[data.isDeleted ? 'inactivo' : 'activo']}
                        sx={{ textTransform: 'capitalize', '& .MuiChip-label': { lineHeight: '18px' } }}
                      />
                    </TableCell>
                    <TableCell style={{ width: "35%" }}>
                      <Typography variant='subtitle2' style={{}}>
                        {data.name.toUpperCase()}
                      </Typography>
                    </TableCell>
                    <TableCell style={{ width: "15%", textAlign: 'center' }}>
                      <AppGetRol name={data.name} />
                    </TableCell>

                  </TableRow>
                ))}
              </TableBody>)}
          </table>
        </TableContainer>
        <Grid item xs={12}>
          <TablePagination
            component="div"
            count={total !== undefined ? total : 100}
            page={page}
            onPageChange={handleChangePage}
            rowsPerPage={pageSize}
            rowsPerPageOptions={[1, 2, 5]}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </Grid>
      </Grid>
    </Card >
  )
}
export default GetAllApp
