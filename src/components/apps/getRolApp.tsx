import { Button, Card, Dialog, Drawer, Grid, TableCell, TableRow, Typography, useMediaQuery, useTheme } from '@mui/material'
import axios from 'axios'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import React, { useEffect, useState } from 'react'
import VisibilityIcon from '@mui/icons-material/Visibility'
import CustomChip from 'src/@core/components/mui/chip'
import { ThemeColor } from 'src/@core/layouts/types'
import Icon from 'src/@core/components/icon'
import DeleteRolUser from '../roles/deleteRolUser';

interface rol {
  _id: string
  rolName: string
  aplicattion: string
  isActive: boolean
}

interface UserStatusType {
  [key: string]: ThemeColor
}
const userStatusObj: UserStatusType = {
  activo: 'success',
  pending: 'warning',
  inactivo: 'error'
}
const AppGetRol: React.FC<{ name: string }> = ({ name }) => {
  const theme = useTheme()
  const fullScreen = useMediaQuery(theme.breakpoints.down('xs'));

  const [rol, setRol] = useState<rol[]>([])
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const GetRol = async () => {
    try {
      const response = await axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}rol?aplicattion=${name}`)
      setRol(response.data.data)
      handleOpen()
    } catch (error: any) {
      toast.error(error.response.data.message)
    }
  }

  return (
    <>
      {findPermission('CENTRAL_EDITAR_ROL') ? (
        <>
          <Button onClick={GetRol}>
            <VisibilityIcon style={{ color: "PaleGreen" }} />
          </Button>
          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 200, sm: 400, md: 600, xl: 1000 } } }}
          >
            <Grid container spacing={2}
              style={{ padding: 10 }}>
              <Grid item xs={12}>
                <Typography variant='h6' textAlign={'left'} fontFamily={'times new roman'}>
                  Roles que Tiene la Aplicacion
                </Typography>
              </Grid>
              <Grid
                item
                xs={12}
              >
                <Card style={{ marginLeft: '10%', width: '80%' }}>
                  <Grid container spacing={2}>
                    <Grid
                      item
                      xs={6}>
                      <Typography
                        variant='body2'
                        textAlign={'center'}
                        style={{ marginTop: 10 }}>
                        Estado
                      </Typography>
                    </Grid>
                    <Grid
                      item
                      xs={6}>
                      <Typography
                        variant='body2'
                        textAlign={'center'}
                        style={{ marginTop: 10 }}>
                        Nombre
                      </Typography>
                    </Grid>
                    {rol.map(rol => (
                      <>
                        <Grid
                          item
                          xs={6}
                          style={{
                            textAlign: 'center',
                            marginBottom: 5
                          }}>
                          <CustomChip
                            skin='light'
                            size='small'
                            label={rol.isActive ? 'activo' : 'inactivo'}
                            color={userStatusObj[rol.isActive ? 'activo' : 'inactivo']}
                            sx={{ textTransform: 'capitalize', '& .MuiChip-label': { lineHeight: '18px' } }}
                          />
                        </Grid>
                        <Grid
                          item
                          xs={6}
                          style={{ marginBottom: 5 }}>
                          <Typography
                            variant='subtitle2'
                            textAlign={'center'}>{rol.rolName}</Typography>
                        </Grid>
                      </>
                    ))}
                  </Grid>

                </Card>
              </Grid>
              <Grid style={{ textAlign: 'left', marginTop: 15, marginLeft: 10 }} item xs={12}>
                <Button
                  onClick={handleClose}
                  sx={{ mb: 2.5 }}
                  variant='contained'
                  style={{ marginLeft: 10 }}>
                  Cerrar
                </Button>
              </Grid>
            </Grid>
          </Drawer>
        </>
      ) : (
        <></>
      )
      }
    </>
  )
}
export default AppGetRol
