import { Button, Card, Dialog, Drawer, Grid, TextField, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useState } from 'react'
import toast from 'react-hot-toast'
import 'react-toastify/dist/ReactToastify.css';
import { UIContext } from 'src/context/ui'
import { findPermission } from '../permisos'
import AddIcon from '@mui/icons-material/Add'

const NewApp = () => {
  const { openData } = useContext(UIContext)
  const [name, setName] = useState<string>()
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
    setName('')
  }
  const handleClose = () => {
    setOpen(false)
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }
  const postData = async () => {
    console.log("name ", name)
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}apps`,
        {
          name: name?.toLowerCase(),
          expiresIn: 'string'
        },
        config
      )
      console.log(response.data)
      setName('')
      openData()
      handleClose()
      toast.success("Aplicacion creada exitosamente")
    } catch (error: any) {
      handleClose()
      toast.error(error.response.data.message)
    }
  }

  return (
    <>
      {findPermission('CENTRAL_CREAR_APP') ? (
        <>
          <Button sx={{ mb: 2.5 }}
            variant='contained'
            style={{ float: 'right', marginTop: 30, marginRight: 20 }} onClick={handleOpen}>
            Nueva App<AddIcon></AddIcon>
          </Button>
          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 200, sm: 400, md: 600, xl: 1000 } } }}
          >
            <Grid container spacing={2} padding={5}>
              <Grid item xs={12}>
                <Typography variant='h6' style={{ marginBottom: 40, marginTop: 20 }}>
                  Nueva Aplicacion
                </Typography>
                <Typography variant='subtitle2' style={{ marginBottom: 40 }}>
                  Coloque el nombre de la nueva aplicacion que desea crear
                </Typography>
              </Grid>
              <Grid item xs={12} style={{ textAlign: 'center', width: '100%' }}>
                <TextField
                  label='Nombre'
                  value={name}
                  onChange={handleChange}
                  style={{ width: '100%' }}
                ></TextField>
              </Grid>
              <Grid style={{ textAlign: 'left', marginTop: 15 }} item xs={12}>
                <Button
                  onClick={() => postData()}
                  sx={{ mb: 2.5 }}
                  variant='contained'
                  style={{
                    marginRight: 10
                  }}>
                  Aceptar
                </Button>
                <Button
                  onClick={handleClose}
                  sx={{ mb: 2.5 }}
                  variant='contained'
                  style={{ marginLeft: 10 }}>
                  Cancelar
                </Button>
              </Grid>
            </Grid>
          </Drawer>
          {/* <Dialog onClose={handleClose} open={open}>
            <Card style={{ padding: 10 }}>
              <Grid container spacing={2} textAlign={'center'}>
                <Grid item xs={12}>
                  <Typography variant='h4' style={{ marginBottom: 40, marginTop: 20 }}>
                    Nueva Aplicacion
                  </Typography>
                  <Typography variant='subtitle2' style={{ marginBottom: 40 }}>
                    Coloque el nombre de la nueva aplicacion que desea crear
                  </Typography>
                  <TextField label='nombre' value={name} onChange={handleChange}></TextField>
                </Grid>
                <Grid item xs={12}
                  style={{ textAlign: 'center' }}>
                  {/* <Button onClick={() => postData()}>Aceptar</Button>
                  <Button onClick={handleClose}>Cancelar</Button> */}
          {/* <Button
            onClick={() => postData()}
            sx={{ mb: 2.5 }}
            variant='contained'
            style={{
              marginRight: 10,
              marginTop: 20
            }}>
            Aceptar
          </Button>
          <Button
            onClick={handleClose}
            sx={{
              mb: 2.5,
              backgroundColor: 'transparent', // Establece el fondo como transparente
              border: '2px solid #ff6961', // Cambia "yourBorderColor" al color deseado
              '&:hover': {
                backgroundColor: 'transparent', // Cambia el fondo al pasar el ratón sobre el botón
              },
            }}
            variant='contained'
            style={{
              marginLeft: 10,
              marginTop: 20
            }}>
            Cancelar
          </Button>
        </Grid>
    </Grid >
            </Card >
          </Dialog >  */}
        </>
      ) : (
        <></>
      )}
    </>
  )
}
export default NewApp
