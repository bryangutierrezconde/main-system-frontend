import { Button, Card, Dialog, Divider, Drawer, Grid, TableCell, TableRow, Typography } from '@mui/material'
import axios from 'axios'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import React, { useEffect, useState } from 'react'
import CancelPresentationIcon from '@mui/icons-material/CancelPresentation'
import VisibilityIcon from '@mui/icons-material/Visibility'
import DeleteRolUser from '../roles/deleteRolUser';
import DeleteAppUser from './deleteAppUser';


interface app {
  _id: string
  name: string
  isDeleted: boolean
  uuid: string
}
interface user {
  id: string
  app: string[]
}
const AppId: React.FC<{ id: string }> = ({ id }) => {
  const [user, setUser] = useState<user>()
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const GetUser = async () => {
    try {
      const response = await axios.get<user>(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/${id}`)
      console.log('response', response.data)
      setUser(response.data)
      console.log(response.data)
      handleOpen()
    } catch (error: any) {
      if (error.response && error.response.status === 400) {
        toast.error(error.response.data.message)
        setTimeout(() => {
          toast.dismiss()
        }, 5000)
      } else {
        toast.error('Ocurrio un error al procesar la solicitud.')
      }
    }
  }

  return (
    <>
      {findPermission('CENTRAL_EDITAR_ROL') ? (
        <>
          <Button
            onClick={GetUser}
            style={{ display: 'block', margin: 'auto' }}>
            <VisibilityIcon style={{ color: 'Aquamarine' }}></VisibilityIcon>
          </Button>
          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 200, sm: 400, md: 600, xl: 1000 } } }}
          >
            <Grid
              container
              spacing={2}
              justifyContent="center"
              alignItems="center"
              style={{
                width: '100%',
                padding: 15
              }}>
              <Grid item xs={12}>
                <Typography variant='h6' textAlign={'left'} >
                  Aplicaciones
                </Typography>
              </Grid>
              <Grid
                item
                xs={12}
              >
                <Card style={{ marginLeft: '10%', width: '80%' }}>
                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <Typography
                        variant='body2'
                        textAlign={'center'}
                        style={{ marginTop: 10 }}>Opciones</Typography>
                    </Grid>
                    <Grid item xs={6}>
                      <Typography
                        variant='body2'
                        textAlign={'center'}
                        style={{ marginTop: 10 }}>Nombre</Typography>
                    </Grid>
                    {user?.app.map((app, index) => (
                      <GetAppName idUser={id} id={app} key={index} />
                    ))}
                  </Grid>
                </Card>

              </Grid>
            </Grid>
          </Drawer>
        </>
      ) : (
        <></>
      )}
    </>
  )
}

const GetAppName: React.FC<{ idUser: string, id: string }> = ({ idUser, id }) => {
  const [app, setApp] = useState<app>()

  useEffect(() => {
    GetName()
  }, [])
  const GetName = async () => {
    try {
      const res = await axios.get<app>(`${process.env.NEXT_PUBLIC_API_CENTRAL}apps/${id}`)
      console.log('res appName', app)
      setApp(res.data)
    } catch (error: any) {
      console.log('error ', error)
    }
  }

  return (
    <>
      <Grid item xs={6}>
        <DeleteAppUser idUser={idUser} rolId={app?.uuid} />
      </Grid>
      <Grid item xs={6}>
        <Typography
          variant='subtitle1'
          textAlign={'center'}>
          {app?.name}
        </Typography>
      </Grid>
    </>
  )
}
export default AppId
