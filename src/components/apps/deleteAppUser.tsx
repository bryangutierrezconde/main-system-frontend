import { Button } from '@mui/material'
import axios from 'axios'
import React, { useContext } from 'react'
import { findPermission } from '../permisos'
import { UIContext } from 'src/context/ui'
import DeleteIcon from '@mui/icons-material/Delete'
import 'react-toastify/dist/ReactToastify.css';
import toast from 'react-hot-toast'

const DeleteAppUser: React.FC<{ idUser: string, rolId: string | undefined }> = ({ idUser, rolId }) => {
  const { openData } = useContext(UIContext)
  const deleteData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.put(`${process.env.NEXT_PUBLIC_API_CENTRAL}user/remove-app-to-user/${idUser}`, {
        rolId: rolId
      }, config)
      console.log(response.data)
      openData()
      toast.success("Aplicacion eliminada exitosamente")
    } catch (error: any) {
      console.log(error)
      toast.error(error.response.data.message)
    }
  }

  return (
    <>
      {findPermission('CENTRAL_ELIMINAR_ROL') ? (
        <>
          <Button
            onClick={deleteData}
            style={{
              display: 'block',
              margin: 'auto'
            }}>
            <DeleteIcon style={{
              color: "IndianRed"
            }}></DeleteIcon>
          </Button>
        </>
      ) : (
        <></>
      )}
    </>
  )
}
export default DeleteAppUser
