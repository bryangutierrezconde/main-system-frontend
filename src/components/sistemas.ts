export function findSystem(name: string) {
  const systemsString = localStorage.getItem('systems') ?? ''

  if (systemsString !== '') {
    const permisos: string[] = JSON.parse(systemsString)
    for (let i = permisos.length - 1; i >= 0; i--) {
      if (permisos[i] === name) {
        return true
        break
      }
    }

    return false
  }
}
