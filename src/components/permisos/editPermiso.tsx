import { Button, Card, Dialog, Drawer, Grid, TextField, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useState } from 'react'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import { UIContext } from 'src/context/ui'
import EditIcon from '@mui/icons-material/Edit'

interface Permission {
  _id: string
  permissionName: string
}
const EditPermiso: React.FC<{ id: string }> = ({ id }) => {
  const { openData } = useContext(UIContext)
  const [name, setName] = useState<string>('')
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }
  const getUser = async () => {
    console.log('getUser')
    try {
      const response = await axios.get<Permission>(`${process.env.NEXT_PUBLIC_API_CENTRAL}permission/${id}`)
      console.log('response', response.data)
      setName(response.data.permissionName)
      console.log(response.data)
      handleOpen()
    } catch (error: any) {
      if (error.response && error.response.status === 400) {
        toast.error(error.response.data.message)
        setTimeout(() => {
          toast.dismiss()
        }, 5000)
      } else {
        toast.error('Ocurrio un error al procesar la solicitud.')
      }
    }
  }
  const updateData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.put<Permission>(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}permission/${id}`,
        {
          permissionName: name
        },
        config
      )
      openData()
      console.log(response.data)
      handleClose()
      toast.success("Permiso editado exitosamente")
    } catch (error: any) {
      toast.error(error.response.data.message)
      handleClose()
    }
  }

  return (
    <>
      {findPermission('CENTRAL_EDITAR_PERMISO') ? (
        <>
          <Button onClick={getUser}>
            <EditIcon
              style={{ color: "#77dd77" }}></EditIcon>
          </Button>
          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 600, md: 600, xl: 1000 } } }}
          >
            <Grid container spacing={2} style={{ padding: 25 }}>
              <Grid item xs={12}>
                <Typography variant='h6' textAlign={'left'}>
                  Nuevo Permiso
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography variant='subtitle2' >
                  Coloque el nombre para el nuevo permiso, puede usar espacios o tambien guion bajo _, no use numeros,
                  intente usar palabras claves, como por ejemplo, ELIMINAR, CREAR, EDITAR, ESTABLECER, seguido del
                  menu que debe tener ese permiso, tambien puede indicar que sistema tendra ese permiso EJEMPLO: CREAR
                  APP CEN o tambien otro ejemplo seria ELIMINAR ROL PERSONAL o tambien ELIMINAR_ROL_PERSONAL
                </Typography>
              </Grid>
              <Grid style={{ textAlign: 'center' }} item xs={12}>
                <TextField
                  fullWidth
                  onChange={handleNameChange}
                  label='Nombre'
                  value={name}
                  style={{ width: '100%', marginTop: 15 }}></TextField>
              </Grid>
            </Grid>
            <Grid style={{ textAlign: 'left', marginTop: 15, marginLeft: 20 }} item xs={12}>
              <Button
                onClick={() => updateData()}
                sx={{ mb: 2.5 }}
                variant='contained'
                style={{ marginRight: 10 }}>
                Aceptar
              </Button>
              <Button
                onClick={handleClose}
                sx={{ mb: 2.5 }}
                variant='contained'
                style={{ marginLeft: 10 }}>
                Cancelar
              </Button>
            </Grid>
          </Drawer>
          {/* <Dialog onClose={handleClose} open={open}>
            <Card style={{ height: '60%', margin: 20, borderColor: 'tomato', borderStyle: 'dotted' }}>
              <Grid container spacing={2} style={{ padding: 25 }}>
                <Grid item xs={12}>
                  <Typography variant='h5' textAlign={'center'}>
                    Nuevo Permiso
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant='subtitle1' >
                    Coloque el nombre para el nuevo permiso, puede usar espacios o tambien guion bajo _, no use numeros,
                    intente usar palabras claves, como por ejemplo, ELIMINAR, CREAR, EDITAR, ESTABLECER, seguido del
                    menu que debe tener ese permiso, tambien puede indicar que sistema tendra ese permiso EJEMPLO: CREAR
                    APP CEN o tambien otro ejemplo seria ELIMINAR ROL PERSONAL o tambien ELIMINAR_ROL_PERSONAL
                  </Typography>
                </Grid>
                <Grid style={{ textAlign: 'center' }} item xs={12}>
                  <TextField fullWidth onChange={handleNameChange} label='nombre' value={name}></TextField>
                </Grid>
              </Grid>
              <Grid style={{ textAlign: 'right' }} item xs={12}>
                <Button onClick={() => updateData()}>Aceptar</Button>
                <Button onClick={handleClose}>Cancelar</Button>
              </Grid>
            </Card>
          </Dialog> */}
        </>
      ) : (
        <></>
      )}
    </>
  )
}
export default EditPermiso
