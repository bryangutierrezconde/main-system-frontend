import { Button, Dialog, Grid, Typography } from '@mui/material'
import axios from 'axios'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { findPermission } from '../permisos'
import { useState } from 'react'
import CancelPresentationIcon from '@mui/icons-material/CancelPresentation'
import VisibilityIcon from '@mui/icons-material/Visibility'

interface PermisionName {
  _id: string
  permissionName: string
}
interface rol {
  _id: string
  rolName: string
  permissionName: PermisionName[]
}
const PermisoId: React.FC<{ id: string }> = ({ id }) => {
  const [name, setName] = useState<rol>()
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }
  const GetPermiso = async () => {
    try {
      const response = await axios.get<rol>(`${process.env.NEXT_PUBLIC_API_CENTRAL}rol/${id}`)
      console.log('response', response.data)
      setName(response.data)
      console.log(response.data)
      handleOpen()
    } catch (error: any) {
      if (error.response && error.response.status === 400) {
        toast.error(error.response.data.message)
        setTimeout(() => {
          toast.dismiss()
        }, 5000)
      } else {
        toast.error('Ocurrio un error al procesar la solicitud.')
      }
    }
  }

  return (
    <>
      {findPermission('CENTRAL_EDITAR_ROL') ? (
        <>
          <Button onClick={GetPermiso}>
            <Typography fontFamily={'times new roman'} variant='subtitle1'>
              Ver permisos del rol
            </Typography>
            <VisibilityIcon></VisibilityIcon>
          </Button>
          <Dialog onClose={handleClose} open={open}>
            <Grid style={{ padding: 20 }} container spacing={2}>
              <Grid item xs={11}>
                <Typography variant='h4' textAlign={'center'} fontFamily={'times new roman'}>
                  permisos de {name?.rolName.toLowerCase()}
                </Typography>
              </Grid>
              <Grid item xs={1}>
                <Button style={{ padding: 5 }} onClick={handleClose}>
                  <CancelPresentationIcon></CancelPresentationIcon>
                </Button>
              </Grid>
              {name !== undefined ? (
                name.permissionName.map(name => (
                  <Grid style={{ borderRadius: 15, margin: 10 }} key={name._id} item xs={12}>
                    <Typography
                      style={{ borderRadius: 15, padding: 15, backgroundColor: 'tomato' }}
                      variant='subtitle1'
                      fontFamily={'times new roman'}
                    >
                      {name.permissionName}
                    </Typography>
                  </Grid>
                ))
              ) : (
                <></>
              )}
            </Grid>
          </Dialog>
        </>
      ) : (
        <></>
      )}
    </>
  )
}
export default PermisoId
