import { Button, Card, Dialog, Drawer, Grid, TextField, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useState } from 'react'
import toast from 'react-hot-toast'
import 'react-toastify/dist/ReactToastify.css';
import { UIContext } from 'src/context/ui'
import { findPermission } from '../permisos'
import AddIcon from '@mui/icons-material/Add'

const NewPermiso = () => {
  const { openData } = useContext(UIContext)
  const [name, setName] = useState<string>()
  const [open, setOpen] = useState(false)
  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }
  const postData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      console.log(config)
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}permission`,
        {
          permissionName: name
        },
        config
      )
      console.log(response.data)
      handleClose()
      openData()
      setName('')
      toast.success("Permiso creado exitosamente")
    } catch (error: any) {
      handleClose()
      toast.error(error.response.data.message)
    }
  }

  return (
    <>
      {findPermission('CENTRAL_CREAR_PERMISO') ? (
        <>
          <Button sx={{ mb: 2.5 }} variant='contained' style={{ float: 'right', marginTop: 30, marginRight: 20 }} onClick={handleOpen}>
            Nuevo Permiso <AddIcon></AddIcon>
          </Button>
          <Drawer
            open={open}
            anchor={'right'}
            onClose={handleClose}
            ModalProps={{ keepMounted: true }}
            sx={{ '& .MuiDrawer-paper': { width: { xs: 300, sm: 600, md: 600, xl: 1000 } } }}
          >
            <Grid container spacing={2} style={{ padding: 25 }}>
              <Grid item xs={12}>
                <Typography variant='h6' textAlign={'left'}>
                  Nuevo Permiso
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography variant='subtitle2'>
                  Coloque el nombre para el nuevo permiso, puede usar espacios o tambien guion bajo _, no use numeros,
                  intente usar palabras claves, como por ejemplo, ELIMINAR, CREAR, EDITAR, ESTABLECER, seguido del
                  menu que debe tener ese permiso, tambien puede indicar que sistema tendra ese permiso EJEMPLO: CREAR
                  APP CEN o tambien otro ejemplo seria ELIMINAR ROL PERSONAL o tambien ELIMINAR_ROL_PERSONAL
                </Typography>
              </Grid>
              <Grid style={{ textAlign: 'center' }} item xs={12}>
                <TextField
                  style={{
                    width: '100%',
                    marginTop: 20,
                    marginBottom: 20
                  }}
                  label='Nombre'
                  value={name}
                  onChange={handleChange}></TextField>
              </Grid>
            </Grid>
            <Grid style={{ textAlign: 'left', marginLeft: 25 }} item xs={12}>
              <Button
                onClick={() => postData()}
                sx={{ mb: 2.5 }}
                variant='contained'
                style={{ marginRight: 10 }}>
                Aceptar
              </Button>
              <Button
                onClick={handleClose}
                sx={{ mb: 2.5, backgroundColor: 'transparent', color: 'red' }}
                variant='contained'
                style={{ marginLeft: 10 }}>
                Cancelar
              </Button>
            </Grid>
          </Drawer>
          {/* <Dialog onClose={handleClose} open={open}>
            <Card style={{ height: '60%', margin: 20, borderColor: 'tomato', borderStyle: 'dotted' }}>
              <Grid container spacing={2} style={{ padding: 25 }}>
                <Grid item xs={12}>
                  <Typography variant='h5' textAlign={'center'}>
                    Nuevo Permiso
                  </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant='subtitle1'>
                    Coloque el nombre para el nuevo permiso, puede usar espacios o tambien guion bajo _, no use numeros,
                    intente usar palabras claves, como por ejemplo, ELIMINAR, CREAR, EDITAR, ESTABLECER, seguido del
                    menu que debe tener ese permiso, tambien puede indicar que sistema tendra ese permiso EJEMPLO: CREAR
                    APP CEN o tambien otro ejemplo seria ELIMINAR ROL PERSONAL o tambien ELIMINAR_ROL_PERSONAL
                  </Typography>
                </Grid>
                <Grid style={{ textAlign: 'center' }} item xs={12}>
                  <TextField
                    style={{
                      width: '70%',
                      marginTop: 20,
                      marginBottom: 20
                    }}
                    label='nombre'
                    value={name}
                    onChange={handleChange}></TextField>
                </Grid>
              </Grid>
              <Grid style={{ textAlign: 'center' }} item xs={12}>
                {/* <Button onClick={() => postData()}>
                  <Typography style={{ backgroundColor: '#ff6c3e', padding: 10, borderRadius: 5 }}>Aceptar</Typography>
                </Button>
                <Button onClick={handleClose}>
                  Cancelar
                </Button> */}
          {/* <Button
                  onClick={() => postData()}
                  sx={{ mb: 2.5 }}
                  variant='contained'
                  style={{ marginRight: 10 }}>
                  Aceptar
                </Button>
                <Button
                  onClick={handleClose}
                  sx={{
                    mb: 2.5,
                    backgroundColor: 'transparent', // Establece el fondo como transparente
                    border: '2px solid #ff6961', // Cambia "yourBorderColor" al color deseado
                    '&:hover': {
                      backgroundColor: 'transparent', // Cambia el fondo al pasar el ratón sobre el botón
                    },
                  }}
                  variant='contained'
                  style={{ marginLeft: 10 }}>
                  Cancelar
                </Button>
              </Grid>
            </Card>
          </Dialog> */}
        </>
      ) : (
        <></>
      )}
    </>
  )
}
export default NewPermiso
