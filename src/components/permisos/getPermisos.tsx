import { Box, Button, Card, Checkbox, CircularProgress, Grid, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow, Typography } from '@mui/material'
import axios from 'axios'
import React, { useContext, useEffect, useState } from 'react'
import { UIContext } from 'src/context/ui'
import DeletePermiso from './deletePermiso'
import EditPermiso from './editPermiso'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';

import SearchIcon from '@mui/icons-material/Search';
import NewPermiso from './newPermiso'
import CustomChip from 'src/@core/components/mui/chip'
import { ThemeColor } from 'src/@core/layouts/types'

interface Permission {
  _id: string
  permissionName: string
  isActive: boolean
}
interface UserStatusType {
  [key: string]: ThemeColor
}
const userStatusObj: UserStatusType = {
  activo: 'success',
  pending: 'warning',
  inactivo: 'error'
}


interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

function CustomTabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

// function a11yProps(index: number) {
//   return {
//     id: `simple-tab-${index}`,
//     'aria-controls': `simple-tabpanel-${index}`,
//   };
// }



const GetAllPermissions: React.FC<{ name: string, title: string, index: number, value: number }> = ({ name, title, index, value }) => {
  const { getData, closeData } = useContext(UIContext)
  const [pageSize, setPageSize] = useState<number>(10)
  const [total, setTotal] = useState<number>()
  const [search, setSearch] = useState<string>('')
  const [page, setPage] = useState(0)
  const [permission, setPermission] = useState<Permission[]>([])
  const [isLoading, SetIsLoading] = useState<boolean>(true)


  const [selectAll, setSelectAll] = useState(false);
  const [selectedPermission, setSelectedPermission] = useState<string[]>([]);

  useEffect(() => {
    get();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageSize, page, search]);

  useEffect(() => {
    if (getData) {
      get();
      closeData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [getData, closeData]);


  useEffect(() => {
    console.log("permissiones actualizados:", selectedPermission);
  }, [selectedPermission]);

  const handleCheckboxChange = (permissionId: string) => {
    const updatedpermissiones = [...selectedPermission];

    if (updatedpermissiones.includes(permissionId)) {
      // Si el permission ya está seleccionado, quítalo
      updatedpermissiones.splice(updatedpermissiones.indexOf(permissionId), 1);
    } else {
      // Si el permission no está seleccionado, agrégalo
      updatedpermissiones.push(permissionId);
    }

    // Actualiza el estado con la nueva lista de permissiones seleccionados
    setSelectedPermission(updatedpermissiones);
  };
  const handleSelectAllChange = () => {
    // Cambia el estado de selectAll
    setSelectAll(!selectAll);

    // Si selectAll es verdadero, selecciona todos los permissiones; de lo contrario, vacía la lista de permissiones seleccionados
    if (!selectAll) {
      const permissions = permission.map((permiso) => permiso._id);
      setSelectedPermission(permissions);
      console.log("permissions", permissions);
    } else {
      setSelectedPermission([]);
      console.log("permissiones", []);
    }
  };


  const handleChangePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
    console.log("handleChangePage ", newPage)
    setPage(newPage)
  }
  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    console.log("handleChangeRowsPerPage ", event.target.value)
    setPageSize(parseInt(event.target.value));
    setPage(0);
  };
  const get = async () => {
    try {
      SetIsLoading(true)
      console.log('limit', pageSize)
      console.log('offset', page + 1)
      console.log('search', search)
      setPermission([])
      const response = await axios.get(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}permission?limit=${pageSize}&offset=${page + 1}&name=${name}_${search}`
      )
      console.log("url: ", process.env.NEXT_PUBLIC_API_CENTRAL, "permission?limit=", pageSize, "&offset=", page + 1, "&search=", search)
      setPermission(response.data.data)
      setTotal(response.data.total)
      console.log(response.data)
      SetIsLoading(false);
    } catch (error: any) {
      toast.error(error.response.data.message)
      console.log(error)
    }
  }


  const FilterName: React.FC<{ name: string }> = ({ name }) => {
    const permission = name.split('_')
    const nameFilter = permission.slice(1).join('_')

    return (<Typography variant='subtitle2'>
      {nameFilter}
    </Typography>)

  }

  return (
    <Card>
      <CustomTabPanel value={value} index={index}>
        <Grid container spacing={1}>
          {/* <Grid item xs={6}>
                <Grid container spacing={2}>
                  <Grid item xs={2} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                    <SearchIcon />
                  </Grid>
                  <Grid item xs={10}>
                    <TextField
                      value={search}
                      onChange={(e) => {
                        setSearch(e.target.value)
                        setPage(0)
                      }}
                      style={{ margin: 20, fontFamily: 'times-new-roman' }}
                    ></TextField>
                  </Grid>
                </Grid>

              </Grid> */}
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <Button sx={{ mb: 2.5 }} variant='contained' style={{ float: 'left', marginTop: 30, marginLeft: 20 }}>
              Mostrar Filtros
            </Button>
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <NewPermiso />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <TableContainer>
            <table style={{ width: '100%', margin: 'auto' }}>
              <TableHead>
                <TableRow>
                  <TableCell style={{ width: '5%' }}>
                    <Checkbox
                      checked={selectAll}
                      onChange={handleSelectAllChange} />
                  </TableCell>
                  <TableCell style={{ width: "15%" }}>
                    <Typography variant='body2' textAlign={'center'}>
                      Estado
                    </Typography>
                  </TableCell>
                  <TableCell style={{ width: "60%" }}>
                    <Typography variant='body2' textAlign={'center'}>
                      Nombre
                    </Typography>
                  </TableCell>
                  <TableCell style={{ width: "20%" }}>
                    <Typography variant='body2' textAlign={'center'}>
                      Opciones
                    </Typography>
                  </TableCell>
                </TableRow>
              </TableHead>

              {isLoading ? (<TableBody>
                <TableRow>
                  <TableCell colSpan={12} align="center">
                    <CircularProgress color='primary' />
                  </TableCell>
                </TableRow>
              </TableBody>) : (<TableBody>
                {permission.map((data) => (
                  <TableRow key={data._id}
                    hover>
                    <TableCell style={{ width: '5%' }}>
                      <Checkbox
                        checked={selectAll || selectedPermission.includes(data._id)}
                        onChange={() => handleCheckboxChange(data._id)} />
                    </TableCell>
                    <TableCell style={{ width: "15%", textAlign: 'center' }}>
                      {/* {data.isActive ? (
                                <Typography textAlign={'center'} style={{ backgroundColor: '#77dd77', borderRadius: 15, padding: 5 }}>
                                  Activo
                                </Typography>
                              ) : (
                                <Typography textAlign={'center'} style={{ backgroundColor: '#ff6961', borderRadius: 15, padding: 5 }}>
                                  Inactivo
                                </Typography>
                              )} */}

                      <CustomChip
                        skin='light'
                        size='small'
                        label={data.isActive ? 'activo' : 'inactivo'}
                        color={userStatusObj[data.isActive ? 'activo' : 'inactivo']}
                        sx={{ textTransform: 'capitalize', '& .MuiChip-label': { lineHeight: '18px' } }}
                      />
                    </TableCell>
                    <TableCell style={{ width: "60%" }}>
                      {/* <Typography variant='subtitle2'>
                                {data.permissionName}
                              </Typography> */}
                      <FilterName name={data.permissionName} />
                    </TableCell>
                    <TableCell style={{ width: "20%", textAlign: 'center' }}>
                      <EditPermiso id={data._id} />
                      <DeletePermiso id={data._id} allId={selectedPermission} />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>)}
            </table>
          </TableContainer>
          <Grid item xs={12}>
            <TablePagination
              component="div"
              count={total !== undefined ? total : 100}
              page={page}
              onPageChange={handleChangePage}
              rowsPerPage={pageSize}
              onRowsPerPageChange={handleChangeRowsPerPage}
            />
          </Grid>
        </Grid>
      </CustomTabPanel>
      {/* <div style={{ width: '100%' }}>
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-contpermissions="panel1a-content"
            id="panel1a-header"
          >
            <Typography variant='h6' textAlign={'center'}>{title}</Typography>
          </AccordionSummary>
          <AccordionDetails>


          </AccordionDetails>
        </Accordion></div> */}
    </Card>
  )
}
export default GetAllPermissions
