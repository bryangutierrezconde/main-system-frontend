export default {
  meEndpoint: '/auth/me',
  loginEndpoint: '/jwt/login',
  registerEndpoint: '/jwt/register',
  storageTokenKeyName: 'accestoken',
  onTokenExpiration: 'refreshToken' // logout | refreshToken
}
