import axios from 'axios'
import jwt from 'jsonwebtoken'

import defaultAuthConfig from 'src/configs/auth'
import mock from 'src/@fake-db/mock'
import { Login } from 'src/services/services'
import router from 'next/router'

// ! These two secrets should be in .env file and not in any other file
const jwtConfig = {
  secret: process.env.NEXT_PUBLIC_JWT_SECRET,
  expirationTime: process.env.NEXT_PUBLIC_JWT_EXPIRATION,
  refreshTokenSecret: process.env.NEXT_PUBLIC_JWT_REFRESH_TOKEN_SECRET
}

interface loginForm {
  id: string
  roles: Roles[]
  email: string
  password: string
  apps: string
  tokenLogin: string
  App: App[]
}
interface App {
  uuid: string
  name: string
  url: string
}
interface Roles {
  _id: string
  rolName: string
  permissionName: string[]
}
interface DecodedToken {
  idUser: string
  App: App[]
  roles: string
}
let success = false

const permissions = async (token: string) => {
  try {
    console.log('auth/decoded')

    // window.localStorage.setItem('TokenLogin', token)
    const response = await axios.post(`${process.env.NEXT_PUBLIC_API_CENTRAL}auth/decoded`, {
      token: token
    })
    console.log('decoded token login roles: ' + response.data.id)

    const id = response.data.id
    window.localStorage.setItem('id', id)

    console.log('roles: ', response.data.roles)

    let permisos
    const permisosLista = []
    for (let j = 0; j < response.data.roles.length; j++) {
      permisos = response.data.roles[j].permissionName
      const keyPermisos = Object.keys(permisos)
      const permisosLength = keyPermisos.length
      console.log('length: ' + permisosLength)

      for (let i = 0; i < permisosLength; i++) {
        console.log('for: ' + i)
        try {
          console.log('auth/decoded')
          const respon = await axios.get(
            `${process.env.NEXT_PUBLIC_API_CENTRAL}permission/${response.data.roles[j].permissionName[i]}`
          )
          permisosLista.push(respon.data.permissionName)
        } catch (error) {
          console.log(error)
        }
      }
    }

    const arrayPermisos = JSON.stringify(permisosLista)
    window.localStorage.setItem('permisos', arrayPermisos)
    console.log('permisos' + arrayPermisos)
  } catch (error) {
    console.log(error)
  }
}
const getDataLogin = async (login: loginForm) => {
  await Login(login)
    .then(async result => {
      // alert(JSON.stringify(result.tokenLogin))
      // alert(JSON.stringify(result.apps))
      window.localStorage.setItem('accestoken', result.tokenLogin)
      console.log('apps', result.apps)

      await permissions(result.tokenLogin)

      // success = true
      const apps = result.apps
      const keyapps = Object.keys(apps)
      const appslength = keyapps.length
      console.log('length: ' + appslength)

      const systems = []

      for (let i = 0; i < appslength; i++) {
        let systemUrl = ''
        console.log('for: ' + i)
        try {
          console.log('auth/login-app' + result.tokenLogin + ' (app)' + result.apps[i])
          const respon = await axios.post(`${process.env.NEXT_PUBLIC_API_CENTRAL}auth/login-app`, {
            token: result.tokenLogin,
            appUuid: result.apps[i]
          })

          // console.log('tokenLogin: ' + result.tokenLogin)
          // console.log('appUuid: ' + result.apps[i])
          // console.log('respon data: ' + respon.data)

          try {
            console.log('auth/decoded')
            const response = await axios.post(`${process.env.NEXT_PUBLIC_API_CENTRAL}auth/decoded`, {
              token: respon.data
            })
            console.log('response.data: ' + response.data.idUser)
            systemUrl = `${response.data.App[0].url}/?id=` + response.data.App[0].uuid + '&token=' + respon.data
            console.log('systemUrl: ' + systemUrl)
            window.localStorage.setItem('token-' + response.data.App[0].name, respon.data)
            window.localStorage.setItem(response.data.App[0].name, systemUrl)

            systems.push(response.data.App[0].name)
          } catch (error) {
            console.log(error)
          }
        } catch (error) {
          console.log(error)
        }
      }

      const arraySystems = JSON.stringify(systems)
      window.localStorage.setItem('systems', arraySystems)
      console.log('systems' + arraySystems)
      success = true
    })
    .catch((e: any) => {
      console.log(e)
      alert(JSON.stringify(e))
      console.error('Código de Estado:', e.response.status)
      console.error('Mensaje de Error:', e.response.data)

      // router.push('/forgot-password')
      success = false
    })

  router.replace('/')

  return success
}
const userD: DecodedToken = {
  idUser: '',
  roles: 'superadmin',
  App: []
}

type ResponseType = [number, { [key: string]: any }]
mock.onPost('/jwt/login').reply(async request => {
  const { email, password, app } = JSON.parse(request.data)

  let error = {
    email: ['Algo salio mal']
  }
  console.log(email + password + app)
  const user = request.data

  if (await getDataLogin(user)) {
    const accessToken = jwt.sign({ id: userD.idUser }, jwtConfig.secret as string, {
      expiresIn: jwtConfig.expirationTime
    })

    const result = {
      accessToken,
      userData: { ...userD, password: undefined }
    }
    console.log(userD.roles + userD.idUser)

    return [200, result]
  } else {
    error = {
      email: ['El correo o la contraseña no son validos']
    }

    return [400, { error }]
  }
})

mock.onGet('/auth/me').reply(config => {
  // ** Get token from header
  // @ts-ignore
  const token = config.headers.Authorization as string

  // ** Default response
  let response: ResponseType = [200, {}]

  // ** Checks if the token is valid or expired

  jwt.verify(token, jwtConfig.secret as string, err => {
    // ** If token is expired
    if (err) {
      // ** If onTokenExpiration === 'logout' then send 401 error
      if (defaultAuthConfig.onTokenExpiration === 'logout') {
        console.log('if')

        // ** 401 response will logout user from AuthContext file
        response = [401, { error: { error: 'Usuario Invalido' } }]
      } else {
        console.log('else')

        // ** If onTokenExpiration === 'refreshToken' then generate the new token
        // const oldTokenDecoded = jwt.decode(token, { complete: true })

        // ** Get user id from old token
        // @ts-ignore
        // const { id: userId } = oldTokenDecoded.payload

        // ** Sign a new token
        // const accessToken = jwt.sign({ id: userId }, jwtConfig.secret as string, {
        //   expiresIn: jwtConfig.expirationTime
        // })
        console.log('3')

        // ** Set new token in localStorage
        const token = window.localStorage.getItem('accestoken')
        if (token !== null) {
          window.localStorage.setItem(defaultAuthConfig.storageTokenKeyName, token)
        }
        const obj = { userData: { ...userD, password: undefined } }
        response = [200, obj]
      }
    } else {
      // ** If token is valid do nothing
      const userData = userD

      // ** return 200 with user data
      response = [200, { userData }]
    }
  })

  return response
})
export default getDataLogin
