---
id: all-users-page
title: All Users Page
sidebar_label: All Users Page
---

En esta página, se encuentra el código fuente para mostrar una tabla con todos los usuarios activos en una aplicación web. A continuación, se proporciona una explicación y documentación de las diferentes secciones del código.

```jsx
import { TableContainer, Paper, Table, TableHead, TableRow, TableCell, TableBody, Tooltip } from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'

interface user {
  id: string
  password: string
  token: string
  roles: string[]
  app: string[]
  name: string[]
  lastname: string
  ci: string
  email: string
  phone: string
  createdAt: string
  updatedAt: string
  isActive: boolean
}

function AllUsersPage() {
  const [user, setUser] = useState<user[]>([])

  // Obtener los datos de los usuarios activos
  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    try {
      const response = await axios.get<user[]>(`${process.env.NEXT_PUBLIC_API_CENTRAL}user`)
      const filteredData = response.data.filter(user => user.isActive === true)
      setUser(filteredData)
      console.log(filteredData)
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nombre</TableCell>
              <TableCell>Apellidos</TableCell>
              <TableCell>Contraseña</TableCell>
              <TableCell>CI</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Celular</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {user.map(user => (
              <TableRow key={user.id}>
                <Tooltip title={user.id}>
                  <TableCell>{user.id.substr(0, 5)}...</TableCell>
                </Tooltip>
                <TableCell>{user.name}</TableCell>
                <TableCell>{user.lastname}</TableCell>
                <Tooltip title={user.password}>
                  <TableCell>{user.password != null ? user.password.substr(0, 15) : 'no hay contraseña'}</TableCell>
                </Tooltip>
                <TableCell>{user.ci}</TableCell>
                <TableCell>{user.email}</TableCell>
                <TableCell>{user.phone}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  )
}

export default AllUsersPage;
```

En este código, se crea un componente llamado AllUsersPage que muestra una tabla con información de todos los usuarios activos en la aplicación. Los datos de los usuarios se obtienen mediante una solicitud HTTP GET utilizando la biblioteca Axios. Se utiliza el estado user para almacenar y actualizar los datos de los usuarios.

La función getData se utiliza para obtener los datos de los usuarios activos desde el servidor. Se realiza una solicitud GET a la URL ${process.env.NEXT_PUBLIC_API_CENTRAL}user, y luego se filtran los usuarios activos utilizando el valor de isActive. Los datos filtrados se almacenan en el estado user.

La tabla se renderiza utilizando los componentes de la biblioteca MUI. Cada fila de la tabla representa un usuario y muestra información como el ID, nombre, apellidos, contraseña parcialmente oculta, CI, email y celular. El componente Tooltip se utiliza para mostrar el ID completo y la contraseña completa al pasar el cursor sobre ellos.

Espero que esta documentación te sea útil. Si tienes alguna pregunta adicional, no dudes en hacerla.
