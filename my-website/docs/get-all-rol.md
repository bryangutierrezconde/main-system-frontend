---
id: get-all-rol
title: Get All Rol
sidebar_label: Get All Rol
---

En esta página, se encuentra el código fuente para obtener y mostrar todos los roles en una aplicación web. A continuación, se proporciona una explicación y documentación de las diferentes secciones del código.

```jsx
import {
  Button,
  Dialog,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField
} from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'

// Interfaz para los roles
interface Rol {
  _id: string
  rolName: string
  permissionName: string[]
}

const GetAllRol = () => {
  const [user, setUser] = useState<Rol[]>([])
  const [name, setName] = useState<string>('')
  const [id, setId] = useState<string>('')
  const [open, setOpen] = useState(false)

  // Función para abrir el diálogo
  const handleOpen = () => {
    setOpen(true)
  }

  // Función para cerrar el diálogo
  const handleClose = () => {
    setOpen(false)
  }

  // Obtener los datos de los roles al cargar la página
  useEffect(() => {
    getData()
  }, [])

  // Función para editar los datos de un rol
  const editData = async (user: Rol) => {
    setName(user.rolName)
    setId(user._id)
    handleOpen()
  }

  // Manejar el cambio en el nombre del rol
  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }

  // Función para actualizar los datos de un rol
  const updateData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.put<Rol>(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}user${id}`,
        {
          name: name
        },
        config
      )
      console.log(response.data)
      handleClose()
      getData()
    } catch (error) {
      console.log(error)
    }
  }

  // Función para obtener los datos de los roles
  const getData = async () => {
    try {
      const response = await axios.get<Rol[]>(`${process.env.NEXT_PUBLIC_API_CENTRAL}rol`)
      setUser(response.data)
      console.log(response.data)
    } catch (error) {
      console.log(error)
    }
  }

  // Función para eliminar un rol
  const deleteData = async (id: string) => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.delete(`${process.env.NEXT_PUBLIC_API_CENTRAL}user${id}`, config)
      console.log(response.data)
      getData()
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nombres</TableCell>
              <TableCell>Opciones</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {user.map(user => (
              <TableRow key={user._id}>
                <TableCell>{user._id}</TableCell>
                <TableCell>{user.rolName}</TableCell>
                <TableCell>
                  <Button onClick={() => deleteData(user._id)}>Eliminar</Button>
                  <br></br>
                  <Button onClick={() => editData(user)}>Editar</Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Dialog onClose={handleClose} open={open}>
        <TextField onChange={handleNameChange} label='nombre' value={name}></TextField>
        <Button onClick={() => updateData()}>Aceptar</Button>
        <Button onClick={handleClose}>Cancelar</Button>
      </Dialog>
    </>
  )
}

export default GetAllRol;
```

En este código, se crea un componente llamado GetAllRol que muestra una tabla con los roles en la aplicación. La lista de roles se obtiene mediante una solicitud HTTP utilizando la biblioteca Axios. Los roles se almacenan en el estado user como un array de objetos de tipo Rol, donde cada objeto tiene propiedades como \_id, rolName y permissionName.

La página muestra la tabla con las columnas ID y Nombres. Por cada rol en la lista, se muestra una fila en la tabla con los detalles correspondientes. Además, se incluyen botones para eliminar y editar cada rol.

Al hacer clic en el botón "Eliminar", se llama a la función deleteData con el ID del rol como argumento. La función realiza una solicitud HTTP DELETE para eliminar el rol del servidor.

Al hacer clic en el botón "Editar", se llama a la función editData con el rol como argumento. La función establece el estado name con el nombre del rol y el estado id con el ID del rol seleccionado. Luego, se abre un diálogo que permite al usuario editar el nombre del rol. Al hacer clic en el botón "Aceptar" en el diálogo, se llama a la función updateData para enviar una solicitud HTTP PUT y actualizar el nombre del rol en el servidor.
