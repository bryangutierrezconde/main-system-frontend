---
id: set-password-page
title: Set Password Page
sidebar_label: Set Password Page
---

En esta página se encuentra el código fuente para crear una contraseña para un usuario en una aplicación web. A continuación, se proporciona una explicación y documentación de las diferentes secciones del código.

```jsx
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Box,
  TextField,
  Button,
  Tooltip
} from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'

interface user {
  id: string
  name: string
  ci: string
  email: string
  password: string
  isActive: boolean
}

const SetPasswordPage: React.FC = () => {
  const [id, setId] = useState<string>()
  const [password, setPassword] = useState<string>()
  const [user, setUser] = useState<user[]>([])

  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    try {
      const response = await axios.get<user[]>(`${process.env.NEXT_PUBLIC_API_CENTRAL}user`)
      const filteredData = response.data.filter(user => user.isActive === true)
      setUser(filteredData)
      console.log(filteredData)
    } catch (error) {
      console.log(error)
    }
  }

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(e.target.value)
  }

  const handleIdChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setId(e.target.value)
  }

  const putData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }

    try {
      const response = await axios.put(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}user/set-password-to-user/${id}`,
        {
          password: password
        },
        config
      )
      console.log(response.data)
      getData()
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nombre</TableCell>
              <TableCell>CI</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Contraseña</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {user.map(user => (
              <TableRow key={user.id}>
                <Tooltip title={user.id}>
                  <TableCell>{user.id.substr(0, 5)}</TableCell>
                </Tooltip>
                <TableCell>{user.name}</TableCell>
                <TableCell>{user.ci}</TableCell>
                <TableCell>{user.email}</TableCell>
                <TableCell>
                  {user.password != null ? user.password.substr(0, 15) : 'No tiene contraseña'}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Box style={{ backgroundColor: 'tomato' }}>
        <h1>Crear una contraseña para un usuario</h1>
        <h2>Coloque el ID del usuario</h2>
        <TextField value={id} onChange={handleIdChange} required label='ID'></TextField>
        <h2>Coloque una contraseña para el usuario</h2>
        <TextField value={password} onChange={handlePasswordChange} required label='Contraseña'></TextField>
        <Button onClick={() => putData()}>Aceptar</Button>
      </Box>
    </>
  )
}

export default SetPasswordPage;
En este código, se crea el componente SetPasswordPage, que permite crear una contraseña para un usuario en la aplicación web. Los datos de los usuarios se obtienen mediante una solicitud HTTP GET utilizando la biblioteca Axios. Se utiliza el estado user para almacenar y actualizar los datos de los usuarios.

La función getData se utiliza para obtener los datos de los usuarios activos desde el servidor. Se realiza una solicitud GET a la URL ${process.env.NEXT_PUBLIC_API_CENTRAL}user, y luego se filtran los usuarios activos utilizando el valor de isActive. Los datos filtrados se almacenan en el estado user.

La tabla se renderiza utilizando los componentes de la biblioteca MUI. Cada fila de la tabla representa un usuario y muestra información como el ID, nombre, CI, email y contraseña. El componente Tooltip se utiliza para mostrar el ID completo al pasar el cursor sobre él.

El componente también incluye un formulario para crear una contraseña para un usuario específico. Hay dos campos de texto, uno para ingresar el ID del usuario y otro para ingresar la contraseña. Los valores de estos campos se almacenan en los estados id y password, respectivamente.

La función handleIdChange se utiliza para manejar los cambios en el campo de texto del ID del usuario. Actualiza el estado id con el valor ingresado.

La función handlePasswordChange se utiliza para manejar los cambios en el campo de texto de la contraseña. Actualiza el estado password con el valor ingresado.

La función putData se utiliza para realizar una solicitud PUT al servidor para establecer la contraseña del usuario. Se envía una solicitud a la URL ${process.env.NEXT_PUBLIC_API_CENTRAL}user/set-password-to-user/${id} con el ID del usuario y la contraseña. Se utiliza el token de acceso obtenido del almacenamiento local como parte de la autorización en los encabezados de la solicitud. Después de realizar la solicitud, se actualizan los datos de los usuarios.

En general, este componente muestra una tabla con información de los usuarios activos y permite establecer una contraseña para un usuario específico.
```
