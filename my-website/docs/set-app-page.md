---
id: set-app-page
title: Set App Page
sidebar_label: Set App Page
---

En esta página, se encuentra el código fuente para asignar una aplicación a un usuario en una aplicación web. A continuación, se proporciona una explicación y documentación de las diferentes secciones del código.

```jsx
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Box,
  TextField,
  Button,
  Tooltip
} from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'

interface user {
  id: string
  name: string
  ci: string
  email: string
  isActive: boolean
  app: string[]
}

const SetAppPage: React.FC = () => {
  const [id, setId] = useState<string>()
  const [app, setApp] = useState<string>()
  const [user, setUser] = useState<user[]>([])

  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    try {
      const response = await axios.get<user[]>(`${process.env.NEXT_PUBLIC_API_CENTRAL}user`)
      const filteredData = response.data.filter(user => user.isActive === true)
      setUser(filteredData)
      console.log(filteredData)
    } catch (error) {
      console.log(error)
    }
  }

  const handleAppChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setApp(e.target.value)
  }

  const handleIdChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setId(e.target.value)
  }

  const putData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }

    try {
      const response = await axios.put(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}user/set-app-to-user/${id}`,
        {
          name: app
        },
        config
      )
      console.log(response.data)
      getData()
      setApp('')
      setId('')
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nombres</TableCell>
              <TableCell>CI</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>Sistemas</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {user.map(user => (
              <TableRow key={user.id}>
                <Tooltip title={user.id}>
                  <TableCell>{user.id.substr(0, 7)}</TableCell>
                </Tooltip>
                <TableCell>{user.name}</TableCell>
                <TableCell>{user.ci}</TableCell>
                <TableCell>{user.email}</TableCell>
                <TableCell>
                  {user.app.map((app, index) => (
                    <p key={index}>{app}</p>
                  ))}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Box style={{ backgroundColor: 'tomato' }}>
        <h1>Asignar una aplicación al usuario</h1>
        <h2>Ingrese el ID del usuario</h2>
        <TextField value={id} onChange={handleIdChange} required label='ID'></TextField>
        <h2>Ingrese una aplicación para el usuario</h2>
        <TextField value={app} onChange={handleAppChange} required label='Aplicación'></TextField>
        <Button onClick={() => putData()}>Aceptar</Button>
      </Box>
    </>
  )
}

export default SetAppPage;
```

En este código, se crea un componente llamado SetAppPage que permite asignar una aplicación a un usuario en la aplicación web. Los datos de los usuarios se obtienen mediante una solicitud HTTP GET utilizando la biblioteca Axios. Se utiliza el estado user para almacenar y actualizar los datos de los usuarios.

La función getData se utiliza para obtener los datos de los usuarios activos desde el servidor. Se realiza una solicitud GET a la URL ${process.env.NEXT_PUBLIC_API_CENTRAL}user, y luego se filtran los usuarios activos utilizando el valor de isActive. Los datos filtrados se almacenan en el estado user.

La tabla se renderiza utilizando los componentes de la biblioteca MUI. Cada fila de la tabla representa un usuario y muestra información como el ID, nombre, CI, email y sistemas asignados. El componente Tooltip se utiliza para mostrar el ID completo alpasar el cursor sobre él.

El componente SetAppPage también incluye un formulario para asignar una aplicación a un usuario. Hay dos campos de texto, uno para ingresar el ID del usuario y otro para ingresar el nombre de la aplicación. Los valores de estos campos se almacenan en los estados id y app, respectivamente.

La función handleIdChange se utiliza para manejar los cambios en el campo de texto del ID del usuario. Actualiza el estado id con el valor ingresado.

La función handleAppChange se utiliza para manejar los cambios en el campo de texto de la aplicación. Actualiza el estado app con el valor ingresado.

La función putData se utiliza para realizar una solicitud PUT al servidor para asignar la aplicación al usuario. Se envía una solicitud a la URL ${process.env.NEXT_PUBLIC_API_CENTRAL}user/set-app-to-user/${id} con el ID del usuario y el nombre de la aplicación. Se utiliza el token de acceso obtenido del almacenamiento local como parte de la autorización en los encabezados de la solicitud. Después de realizar la solicitud, se actualizan los datos de los usuarios y se restablecen los valores de los campos de texto id y app.

En general, este componente muestra una tabla con información de los usuarios activos y permite asignar una aplicación a un usuario específico.
