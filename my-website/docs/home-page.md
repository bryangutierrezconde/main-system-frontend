---
id: home-page
title: Home Page
sidebar_label: Home Page
---

En esta página, se encuentra el código fuente para la página principal de tu aplicación web. A continuación, se proporciona una explicación y documentación de las diferentes secciones del código.

```jsx
// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import { Button } from '@mui/material'
import { Login } from 'src/services/services'
import Counter from 'src/redux/slices/counter/counter'
import Jwt from 'jsonwebtoken'

// Definición de la interfaz para el token decodificado
interface DecodedToken {
  id: string
  roles: string[]
}

const Home = () => {
  // Función para obtener los datos de inicio de sesión
  const getDataLogin = async () => {
    // Datos de inicio de sesión
    const form = {
      email: 'string@gmail.com',
      password: '12345678'
    }

    // Acceso a la variable de entorno
    alert(process.env.NEXT_PUBLIC_API_CENTRAL)

    // Llamada a la función de inicio de sesión
    await Login(form)
      .then(result => {
        // Alertas con los resultados de la llamada
        alert(JSON.stringify(result))
        alert(JSON.stringify(result.token))

        // Deserialización del token
        deserealizeToken(result.token)

        // Guardar el token en el localStorage
        //localStorage.setItem('token', result.response.token)
      })
      .catch(e => {
        alert(JSON.stringify('error desconocido' + e))
      })

    // Función para deserializar el token
    function deserealizeToken(token: string): DecodedToken | null {
      try {
        const decodedToken = Jwt.verify(token, 'semillaSecreta') as DecodedToken
        alert(JSON.stringify(decodedToken))

        if (decodedToken) {
          const { id, roles } = decodedToken

          // Utilizar los datos del token
          console.log('id', id)
          console.log('roles', roles)
        }

        return decodedToken
      } catch (error) {
        console.error('error al deserializar token', error)

        return null
      }
    }
  }

  return (
    <Grid container spacing={6}>
      <Grid item xs={12}>
        {/* Componente Counter */}
        <Counter />

        {/* Botón para obtener los datos de inicio de sesión */}
        <Button onClick={getDataLogin}>CLICK</Button>

        {/* Tarjeta 1 */}
        <Card>
          <CardHeader title='Kick start your project 🚀'></CardHeader>
          <CardContent>
            <Typography sx={{ mb: 2 }}>All the best for your new project.</Typography>
            <Typography>
              Please make sure to read our Template Documentation to understand where to go from here and how to use our
              template.
            </Typography>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12}>
        {/* Tarjeta 2 */}
        <Card>
          <CardHeader title='ACL and JWT 🔒'></CardHeader>
          <CardContent>
            <Typography sx={{ mb: 2 }}>
              Access Control (ACL) and Authentication (JWT) are the two main security features of our template and are
              implemented in the starter-kit as well.
            </Typography>
            <Typography>Please read our Authentication and ACL Documentations to get more out of them.</Typography>
          </CardContent>
        </Card>
      </Grid>
    </Grid>
  )
}

export default Home
```
