`jwt.md`:

````markdown
---
id: jwt
title: JWT
---

---

## sidebar_position: 2

## Descripción

JWT (JSON Web Token) es un estándar abierto para la creación de tokens de acceso seguro y compactos entre diferentes partes.

## Instalación

Para utilizar JWT en tu proyecto, puedes instalar la biblioteca `jsonwebtoken` mediante npm o yarn:

```bash
npm install jsonwebtoken
```
````

Generación de tokens
A continuación se muestra un ejemplo de cómo generar un token JWT en Node.js:
import jwt from 'jsonwebtoken';

const payload = { userId: 123 };

const token = jwt.sign(payload, 'mi_secreto');
console.log(token);
