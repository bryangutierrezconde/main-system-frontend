---
id: new-rol
title: New Rol
sidebar_label: New Rol
---

En esta página, se encuentra el código fuente para agregar un nuevo rol en una aplicación web. A continuación, se proporciona una explicación y documentación de las diferentes secciones del código.

```jsx
import { Button, Dialog, TextField } from '@mui/material'
import axios from 'axios'
import React, { useState } from 'react'

const NewRol = () => {
  const [name, setName] = useState<string>()
  const [open, setOpen] = useState(false)

  // Función para abrir el diálogo
  const handleOpen = () => {
    setOpen(true)
  }

  // Función para cerrar el diálogo
  const handleClose = () => {
    setOpen(false)
  }

  // Manejar el cambio en el nombre del rol
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }

  // Función para enviar los datos del nuevo rol al servidor
  const postData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}rol`,
        {
          rolName: name
        },
        config
      )
      console.log(response.data)
      handleClose()
      window.location.reload()
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <>
      <Button onClick={handleOpen}>Nuevo Rol</Button>
      <Dialog onClose={handleClose} open={open}>
        <TextField label='nombre' value={name} onChange={handleChange}></TextField>
        <Button onClick={() => postData()}>Aceptar</Button>
        <Button onClick={handleClose}>Cancelar</Button>
      </Dialog>
    </>
  )
}

export default NewRol;
```

En este código, se crea un componente llamado NewRol que muestra un botón para agregar un nuevo rol en la aplicación. Al hacer clic en el botón "Nuevo Rol", se abre un diálogo que permite al usuario ingresar el nombre del nuevo rol. El nombre del rol se almacena en el estado name.

La función handleOpen se utiliza para abrir el diálogo estableciendo el estado open en true. La función handleClose se utiliza para cerrar el diálogo estableciendo el estado open en false.

La función handleChange se utiliza para manejar el cambio en el campo de texto del nombre del rol, actualizando el estado name con el valor ingresado por el usuario.

La función postData se utiliza para enviar los datos del nuevo rol al servidor mediante una solicitud HTTP POST utilizando la biblioteca Axios. Se incluye el nombre del rol en el cuerpo de la solicitud. Si la solicitud se realiza correctamente, se cierra el diálogo y se recarga la página.

Espero que esta documentación te sea útil. Si tienes alguna pregunta adicional, no dudes en hacerla.
