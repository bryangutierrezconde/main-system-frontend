---
sidebar_position: 1
---

Introducción al Tutorial
Descubre Docusaurus en menos de 5 minutos.

Empezando
Comienza por crear un nuevo sitio.

O prueba Docusaurus de inmediato con docusaurus.new.

Lo que necesitarás
Node.js versión 16.14 o superior:
Al instalar Node.js, se recomienda marcar todas las casillas relacionadas con las dependencias.
Generar un nuevo sitio
Genera un nuevo sitio de Docusaurus usando la plantilla clásica.

La plantilla clásica se agregará automáticamente a tu proyecto después de ejecutar el siguiente comando:

```bash
npm init docusaurus@latest my-website classic
```

Puedes escribir este comando en la línea de comandos (Command Prompt), PowerShell, Terminal o cualquier otra terminal integrada en tu editor de código.

El comando también instala todas las dependencias necesarias para ejecutar Docusaurus.

Iniciar el sitio
Ejecuta el servidor de desarrollo:

```bash
cd my-website
npm run start
```

El comando cd cambia el directorio en el que estás trabajando. Para trabajar con tu sitio Docusaurus recién creado, necesitarás navegar a través de la terminal hasta ese directorio.

El comando npm run start genera tu sitio web de forma local y lo sirve a través de un servidor de desarrollo, listo para que lo veas en http://localhost:3000/.

Abre docs/intro.md (esta página) y edita algunas líneas: el sitio se recarga automáticamente y muestra tus cambios.
Introducción al Sistema Central
El Sistema Central es una aplicación web diseñada para cumplir con diversas funciones en un entorno empresarial. Proporciona una interfaz intuitiva y fácil de usar para gestionar diferentes aspectos del negocio.

Componentes del Sistema Central
El Sistema Central cuenta con los siguientes componentes principales:

Autenticación y Autorización: Permite a los usuarios autenticarse en el sistema y gestionar los permisos de acceso a diferentes áreas y funcionalidades.

Gestión de Usuarios: Permite administrar los usuarios del sistema, agregar nuevos usuarios, asignar roles y gestionar sus permisos.

Gestión de Aplicaciones: Permite crear y administrar diferentes aplicaciones dentro del sistema, configurando sus características y permisos.

Gestión de Datos: Proporciona herramientas para importar, exportar y gestionar los datos utilizados por las aplicaciones del sistema.

Panel de Control: Ofrece una visión general de las métricas y estadísticas importantes para el negocio, brindando información en tiempo real.

Seguridad: Implementa medidas de seguridad para proteger los datos y garantizar la integridad del sistema.

Descripción General
El Sistema Central es una solución completa que facilita la gestión y administración de múltiples aspectos del negocio. Con su interfaz intuitiva y sus potentes funcionalidades, ayuda a mejorar la eficiencia y productividad de la empresa.

Explora la documentación para conocer en detalle cada uno de los componentes y funcionalidades del Sistema Central.

¡Comienza a aprovechar al máximo el Sistema Central y optimiza tus procesos empresariales!

---
