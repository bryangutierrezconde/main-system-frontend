---
id: axios
title: Axios
---

## Axios

Axios es una biblioteca de JavaScript para realizar peticiones HTTP desde el navegador o desde Node.js.

## Instalación

Para utilizar Axios, primero debes instalarlo en tu proyecto utilizando npm o yarn:

```bash
npm install axios
```

Uso básico
A continuación, se muestra un ejemplo básico de cómo utilizar Axios para realizar una solicitud GET:
import axios from 'axios';

axios.get('https://api.example.com/data')
.then(response => {
console.log(response.data);
})
.catch(error => {
console.error(error);
});
