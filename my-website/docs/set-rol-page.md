---
id: set-rol-page
title: Set Rol Page
sidebar_label: Set Rol Page
---

En esta página se encuentra el código fuente para asignar un rol a un usuario en una aplicación web. A continuación, se proporciona una explicación y documentación de las diferentes secciones del código.

```jsx
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Box,
  TextField,
  Button,
  Tooltip
} from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'

interface user {
  id: string
  name: string
  ci: string
  email: string
  isActive: boolean
  roles: string[]
  rol: string[]
}

interface roles {
  _id: string
  rolName: string
  permissionName: string[]
}

const SetRolPage: React.FC = () => {
  const [id, setId] = useState<string>()
  const [rol, setRol] = useState<string>()
  const [user, setUser] = useState<user[]>([])

  useEffect(() => {
    getData()
    getRol()
  }, [])

  const getData = async () => {
    try {
      const response = await axios.get<user[]>(`${process.env.NEXT_PUBLIC_API_CENTRAL}user`)
      const filteredData = response.data.filter(user => user.isActive === true)
      setUser(filteredData)
      console.log(filteredData)
    } catch (error) {
      console.log(error)
    }
  }

  const getRol = () => {
    for (let i = user.length - 1; i > 0; i--) {
      for (let j = user[i].roles.length - 1; j > 0; j--) {
        const response = axios.get(`${process.env.NEXT_PUBLIC_API_CENTRAL}rol/${user[i].roles[j]}`)
        console.log(user[i].id + ' roles: ' + response)
      }
    }
  }

  const handleRolChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setRol(e.target.value)
  }

  const handleIdChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setId(e.target.value)
  }

  const putData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }

    try {
      const response = await axios.put(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}user/set-rol-to-user/${id}`,
        {
          name: rol
        },
        config
      )
      console.log(response.data)
      getData()
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>NOMBRE</TableCell>
              <TableCell>CI</TableCell>
              <TableCell>EMAIL</TableCell>
              <TableCell>Roles</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {user.map(user => (
              <TableRow key={user.id}>
                <Tooltip title={user.id}>
                  <TableCell>{user.id.substr(0, 7)}</TableCell>
                </Tooltip>
                <TableCell>{user.name}</TableCell>
                <TableCell>{user.ci}</TableCell>
                <TableCell>{user.email}</TableCell>
                <TableCell>
                  {user.roles.map((rol, index) => (
                    <p key={index}>{rol}</p>
                  ))}
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Box style={{ backgroundColor: 'tomato' }}>
        <h1>Asignar un rol al usuario</h1>
        <h2>Coloque el ID del usuario</h2>
        <TextField value={id} onChange={handleIdChange} required label='ID'></TextField>
        <h2>Coloque un Rol para el usuario</h2>
        <TextField value={rol} onChange={handleRolChange} required label='Roles'></TextField>
        <Button onClick={() => putData()}>Aceptar</Button>
      </Box>
    </>
  )
}

export default SetRolPage;
```

En este código, se crea el componente SetRolPage, que permite asignar un rol a un usuario en la aplicación web. Los datos de los usuarios se obtienen mediante una solicitud HTTP GET utilizando la biblioteca Axios. Se utiliza el estado user para almacenar y actualizar los datos de los usuarios.

La función getData se utiliza para obtener los datos de los usuarios activos desde el servidor. Se realiza una solicitud GET a la URL ${process.env.NEXT_PUBLIC_API_CENTRAL}user, y luego se filtran los usuarios activos utilizando el valor de isActive. Los datos filtrados se almacenan en el estado user.

La función getRol se utiliza para obtener los detalles de los roles de los usuarios. Se recorre el arreglo de usuarios y, para cada usuario, se recorre el arreglo de roles. Se realiza una solicitud GET a la URL ${process.env.NEXT_PUBLIC_API_CENTRAL}rol/${user[i].roles[j]} para obtener los detalles de cada rol. Los detalles se muestran en la consola para fines de depuración.

La tabla se renderiza utilizando los componentes de la biblioteca MUI. Cada fila de la tabla representa un usuario y muestra información como el ID, nombre, CI, email y roles asignados. El componente Tooltip se utiliza para mostrar el ID completo al pasar el cursor sobre él.

El componente también incluye un formulario para asignar un rol a un usuario específico. Hay dos campos de texto, uno para ingresar el ID del usuario y otro para ingresar el rol. Los valores de estos campos se almacenan en los estados id y rol, respectivamente.

La función handleIdChange se utiliza para manejar los cambios en el campo de texto del ID del usuario. Actualiza el estado id con el valor ingresado.

La función handleRolChange se utiliza para manejar los cambios en el campo de texto del rol. Actualiza el estado rol con el valor ingresado.

La función putData se utiliza para realizar una solicitud PUT al servidor para asignar el rol al usuario. Se envía una solicitud a la URL ${process.env.NEXT_PUBLIC_API_CENTRAL}user/set-rol-to-user/${id} con el ID del usuario y el rol. Se utiliza el token de acceso obtenido del almacenamiento local como parte de la autorización en los encabezados de la solicitud. Después de realizar la solicitud, se actualizan los datos de los usuarios.

En general, este componente muestra una tabla con información de los usuarios activos y permite asignar unrol a un usuario específico.
