---
id: apps-page
title: Apps Page
sidebar_label: Apps Page
---

En esta página, se encuentra el código fuente para una página de aplicaciones en una aplicación web. A continuación, se proporciona una explicación y documentación de las diferentes secciones del código.

```jsx
import {
  Box,
  Button,
  Dialog,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField
} from '@mui/material'
import axios from 'axios'
import React, { useEffect, useState } from 'react'

// Interfaz para las aplicaciones
interface App {
  _id: string
  name: string
  isDeleted: boolean
  uuid: string
}

const AppsPage = () => {
  const [id, setId] = useState<string>('')
  const [name, setName] = useState<string>('')
  const [app, setApp] = useState<App[]>([])
  const [open, setOpen] = useState(false)

  // Función para abrir el diálogo
  const handleOpen = () => {
    setOpen(true)
  }

  // Función para cerrar el diálogo
  const handleClose = () => {
    setOpen(false)
  }

  // Obtener los datos de las aplicaciones al cargar la página
  useEffect(() => {
    getData()
  }, [])

  // Función para editar los datos de una aplicación
  const editData = async (app: App) => {
    setName(app.name)
    setId(app._id)
    handleOpen()
  }

  // Manejar el cambio en el nombre de la aplicación
  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setName(e.target.value)
  }

  // Función para crear una nueva aplicación
  const postData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.post(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}apps`,
        {
          name: name
        },
        config
      )
      console.log(response.data)
      getData()
      setName('')
    } catch (error) {
      console.log(error)
    }
  }

  // Función para actualizar los datos de una aplicación
  const updateData = async () => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.put<App>(
        `${process.env.NEXT_PUBLIC_API_CENTRAL}apps/update-app${id}`,
        {
          name: name
        },
        config
      )
      console.log(response.data)
      handleClose()
      getData()
    } catch (error) {
      console.log(error)
    }
  }

  // Función para obtener los datos de las aplicaciones
  const getData = async () => {
    try {
      const response = await axios.get<App[]>(`${process.env.NEXT_PUBLIC_API_CENTRAL}apps`)
      setApp(response.data)
      console.log(response.data)
    } catch (error) {
      console.log(error)
    }
  }

  // Función para eliminar una aplicación
  const deleteData = async (id: string) => {
    const token = window.localStorage.getItem('accestoken')
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'application/json'
      }
    }
    try {
      const response = await axios.delete(`${process.env.NEXT_PUBLIC_API_CENTRAL}apps/delete-app${id}`, config)
      console.log(response.data)
      getData()
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label='simple table'>
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell>Nombres</TableCell>
              <TableCell>Estado</TableCell>
              <TableCell>Opciones</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {app.map(app => (
              <TableRow key={app._id}>
                <TableCell>{app._id}</TableCell>
                <TableCell>{app.name}</TableCell>
                <Button>{app.isDeleted ? 'Activar' : 'Desactivar'}</Button>
                <TableCell>
                  <Button onClick={() => deleteData(app._id)}>Eliminar</Button>
                  <br></br>
                  <Button onClick={() => editData(app)}>Editar</Button>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Dialog onClose={handleClose} open={open}>
        <TextField onChange={handleNameChange} label='nombre' value={name}></TextField>
        <Button onClick={() => updateData()}>Aceptar</Button>
        <Button onClick={handleClose}>Cancelar</Button>
      </Dialog>
      <Box style={{ backgroundColor: 'tomato' }}>
        <h1>Crear una nueva aplicación</h1>
        <h2>Coloque un nombre para la aplicación</h2>
        <TextField value={name} onChange={handleNameChange} required label='Aplicación'></TextField>
        <Button onClick={() => postData()}>Aceptar</Button>
      </Box>
    </>
  )
}

export default AppsPage;
```

En este código, se crea una página que muestra una tabla de aplicaciones. La lista de aplicaciones se obtiene mediante una solicitud HTTP utilizando la biblioteca Axios. Las aplicaciones se almacenan en el estado app como un array de objetos de tipo App, donde cada objeto tiene propiedades como \_id, name, isDeleted y uuid.

La página muestra la tabla con las columnas ID, Nombres, Estado y Opciones. Por cada aplicación en la lista, se muestra una fila en la tabla con los detalles correspondientes. Además, se incluyen botones para eliminar y editar cada aplicación.

Al hacer clic en el botón "Eliminar", se llama a la función deleteData con el ID de la aplicación como argumento. La función realiza una solicitud HTTP DELETE para eliminar la aplicación del servidor.

Al hacer clic en el botón "Editar", se llama a la función editData con la aplicación como argumento. La función establece el estado name con el nombre de la aplicación y el estado id con el ID de la aplicación seleccionada. Luego, se abre un diálogo que permite al usuario editar el nombre de la aplicación. Al hacer clic en el botón "Aceptar" en el diálogo, se llama a la función updateData para enviar una solicitud HTTP PUT y actualizar el nombre de la aplicación en el servidor.

Además, se incluye un formulario en la parte inferior de la página que permite crear una nueva aplicación. Al ingresar un nombre en el campo de texto y hacer clic en el botón "Aceptar", se llama
